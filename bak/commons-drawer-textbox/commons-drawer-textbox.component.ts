import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsEditorService } from 'ngx-materialcommons-es-keyboard';
import { ICommonsKeyboard } from 'ngx-materialcommons-es-keyboard';

@Component({
		selector: 'commons-drawer-textbox',
		templateUrl: './commons-drawer-textbox.component.html',
		styleUrls: ['./commons-drawer-textbox.component.less']
})
export class CommonsDrawerTextboxComponent extends CommonsComponent {
	@Input() type: string = 'text';
	@Input() disabled: boolean = false;
	@Input() placeholder?: string;
	
	@Input() keyboard!: ICommonsKeyboard;

	@Output() valueChange: EventEmitter<string> = new EventEmitter<string>();
	@Input() value: string|undefined;

	constructor(
			private editorService: CommonsEditorService
	) {
		super();
	}

	doEdit(): void {
		this.editorService.edit(
				this.value === undefined ? '' : this.value,
				{
						keyboard: this.keyboard
				},
				(v: string): void => {
					this.valueChange.emit(v);
				},
				(): void => {
					// do nothing
				}
		);
	}
}
