import { Component } from '@angular/core';

import { ECommonsTableSelectable } from 'ngx-materialcommons-es-table';

@Component({
		selector: 'app-root',
		templateUrl: './app.component.html',
		styleUrls: ['./app.component.less']
})
export class AppComponent {
	ECommonsTableSelectable = ECommonsTableSelectable;
	title = 'MaterialCommonsEs';
	
	onswitch: boolean = false;
	slideValue: number|string = 4;

	showPanel1: boolean = false;
	showPanel2: boolean = false;
	showPanel3: boolean = true;
}
