import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Injectable, NgModule } from '@angular/core';
import { HammerModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import * as Hammer from 'hammerjs';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';
import { NgxAngularCommonsEsBrowserModule } from 'ngx-angularcommons-es-browser';
import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsGraphicsModule } from 'ngx-materialcommons-es-graphics';
import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsMobileModule } from 'ngx-materialcommons-es-mobile';
import { NgxMaterialCommonsEsFormModule } from 'ngx-materialcommons-es-form';
import { NgxMaterialCommonsEsTableModule } from 'ngx-materialcommons-es-table';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
	overrides = {
			swipe: { direction: Hammer.DIRECTION_ALL }
	};
}

@NgModule({
		imports: [
				BrowserModule,
				BrowserAnimationsModule,
				HammerModule,
				NgxAngularCommonsEsCoreModule.forRoot(),
				NgxAngularCommonsEsAppModule.forRoot(),
				NgxAngularCommonsEsBrowserModule.forRoot(),
				NgxAngularCommonsEsPipeModule,
				NgxMaterialCommonsEsCoreModule.forRoot(),
				NgxMaterialCommonsEsGraphicsModule,
				NgxMaterialCommonsEsAppModule.forRoot(),
				NgxMaterialCommonsEsMobileModule.forRoot(),
				NgxMaterialCommonsEsFormModule.forRoot(),
				NgxMaterialCommonsEsTableModule.forRoot(),
				AppRoutingModule
		],
		declarations: [
				AppComponent
		],
		providers: [
				{
						provide: HAMMER_GESTURE_CONFIG,
						useClass: MyHammerConfig
				}
		],
		bootstrap: [ AppComponent ]
})
export class AppModule {}
