/*
 * Public API Surface of ngx-materialcommons-es-graphics
 */

export * from './lib/components/commons-wait-rotate/commons-wait-rotate.component';

export * from './lib/ngx-materialcommons-es-graphics.module';
