import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsWaitRotateComponent } from './components/commons-wait-rotate/commons-wait-rotate.component';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [
				CommonsWaitRotateComponent
		],
		exports: [
				CommonsWaitRotateComponent
		]
})
export class NgxMaterialCommonsEsGraphicsModule { }
