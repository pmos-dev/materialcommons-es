import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-info-line',
		templateUrl: './commons-info-line.component.html',
		styleUrls: ['./commons-info-line.component.less']
})
export class CommonsInfoLineComponent extends CommonsComponent {
	@Input() allowIgnore: boolean = false;
	
	@Output() ignore: EventEmitter<void> = new EventEmitter<void>();
}
