import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-button]',
		templateUrl: './commons-button.component.html',
		styleUrls: ['./commons-button.component.less']
})
export class CommonsButtonComponent extends CommonsComponent {}
