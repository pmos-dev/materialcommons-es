import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-pane',
		templateUrl: './commons-pane.component.html',
		styleUrls: ['./commons-pane.component.less']
})
export class CommonsPaneComponent extends CommonsComponent {
	@Input() title: string|undefined;
}
