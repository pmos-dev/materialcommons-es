import { Injectable } from '@angular/core';
import { Renderer2, RendererFactory2 } from '@angular/core';

import { ReplaySubject, Observable } from 'rxjs';

import { CommonsConfigService } from 'ngx-angularcommons-es-app';

import { TCommonsThemeConfig, isTCommonsThemeConfig } from '../types/tcommons-theme-config';

import { ECommonsThemeColor, ECOMMONS_THEME_COLORS } from '../enums/ecommons-theme-color';
import { ECommonsThemeContrast } from '../enums/ecommons-theme-contrast';

@Injectable({
		providedIn: 'root'
})
export class CommonsThemeService {
	private onColorChanged: ReplaySubject<ECommonsThemeColor|undefined> = new ReplaySubject<ECommonsThemeColor|undefined>(1);
	private onContrastChanged: ReplaySubject<ECommonsThemeContrast|undefined> = new ReplaySubject<ECommonsThemeContrast|undefined>(1);
	
	private renderer: Renderer2;
	
	private currentColor: ECommonsThemeColor|undefined;
	private currentContrast: ECommonsThemeContrast|undefined;
	
	constructor(
			rendererFactory: RendererFactory2,
			private configService: CommonsConfigService
	) {
		this.renderer = rendererFactory.createRenderer(null, null);
	}
	
	public loadConfigDefault(): boolean {
		const defaultTheme: TCommonsThemeConfig|undefined = this.configService.getObject('theme', 'default');
		
		if (!defaultTheme || !isTCommonsThemeConfig(defaultTheme)) return false;
		
		this.setCurrentColor(defaultTheme.color);
		this.setCurrentContrast(defaultTheme.contrast);
		
		return true;
	}
	
	public getCurrentColor(): ECommonsThemeColor|undefined {
		return this.currentColor;
	}
	
	public getCurrentContrast(): ECommonsThemeContrast|undefined {
		return this.currentContrast;
	}
	
	public setCurrentColor(color: ECommonsThemeColor): void {
		this.currentColor = color;
		
		this.renderer.addClass(document.body, `materialcommons-theme-${color}`);
		
		for (const c of ECOMMONS_THEME_COLORS) {
			if (c !== color) {
				this.renderer.removeClass(document.body, `materialcommons-theme-${c}`);
			}
		}
		
		this.onColorChanged.next(color);
	}
	
	public setCurrentContrast(contrast: ECommonsThemeContrast): void {
		this.currentContrast = contrast;
		
		switch (contrast) {
		case ECommonsThemeContrast.LIGHT:
			this.renderer.addClass(document.body, 'theme-light');
			this.renderer.removeClass(document.body, 'theme-dark');
			break;
		case ECommonsThemeContrast.DARK:
			this.renderer.addClass(document.body, 'theme-dark');
			this.renderer.removeClass(document.body, 'theme-light');
			break;
		}
		
		this.onContrastChanged.next(contrast);
	}
	
	public colorChangedObservable(): Observable<ECommonsThemeColor|undefined> {
		return this.onColorChanged;
	}
	
	public contrastChangedObservable(): Observable<ECommonsThemeContrast|undefined> {
		return this.onContrastChanged;
	}
}
