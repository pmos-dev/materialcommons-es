import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsAppModule } from 'ngx-angularcommons-es-app';

import { CommonsPaneComponent } from './components/commons-pane/commons-pane.component';
import { CommonsButtonComponent } from './components/commons-button/commons-button.component';
import { CommonsButtonBarComponent } from './components/commons-button-bar/commons-button-bar.component';
import { CommonsInfoLineComponent } from './components/commons-info-line/commons-info-line.component';
import { CommonsDetailsComponent } from './components/commons-details/commons-details.component';
import { CommonsDetailLabelComponent } from './components/commons-detail-label/commons-detail-label.component';
import { CommonsDetailTextComponent } from './components/commons-detail-text/commons-detail-text.component';
import { CommonsBodyBackgroundComponent } from './components/commons-body-background/commons-body-background.component';

import { CommonsThemeService } from './services/commons-theme.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsAppModule,
				NgxAngularCommonsEsCoreModule
		],
		declarations: [
				CommonsPaneComponent,
				CommonsInfoLineComponent,
				CommonsButtonComponent,
				CommonsButtonBarComponent,
				CommonsDetailsComponent,
				CommonsDetailLabelComponent,
				CommonsDetailTextComponent,
				CommonsBodyBackgroundComponent
		],
		exports: [
				CommonsPaneComponent,
				CommonsInfoLineComponent,
				CommonsButtonComponent,
				CommonsButtonBarComponent,
				CommonsDetailsComponent,
				CommonsDetailLabelComponent,
				CommonsDetailTextComponent,
				CommonsBodyBackgroundComponent
		]
})
export class NgxMaterialCommonsEsCoreModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsCoreModule> {
		return {
				ngModule: NgxMaterialCommonsEsCoreModule,
				providers: [
						CommonsThemeService
				]
		};
	}
}
