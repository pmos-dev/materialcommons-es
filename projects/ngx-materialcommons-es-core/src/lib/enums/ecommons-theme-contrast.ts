import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsThemeContrast {
		LIGHT = 'light',
		DARK = 'dark'
}

export function toECommonsThemeContrast(type: string): ECommonsThemeContrast|undefined {
	switch (type) {
	case ECommonsThemeContrast.LIGHT.toString():
		return ECommonsThemeContrast.LIGHT;
	case ECommonsThemeContrast.DARK.toString():
		return ECommonsThemeContrast.DARK;
	}
	return undefined;
}

export function fromECommonsThemeContrast(type: ECommonsThemeContrast): string {
	switch (type) {
	case ECommonsThemeContrast.LIGHT:
		return ECommonsThemeContrast.LIGHT.toString();
	case ECommonsThemeContrast.DARK:
		return ECommonsThemeContrast.DARK.toString();
	}
	
	throw new Error('Unknown ECommonsThemeContrast');
}

export function isECommonsThemeContrast(test: unknown): test is ECommonsThemeContrast {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsThemeContrast(test) !== undefined;
}

export function keyToECommonsThemeContrast(key: string): ECommonsThemeContrast {
	switch (key) {
	case 'LIGHT':
		return ECommonsThemeContrast.LIGHT;
	case 'DARK':
		return ECommonsThemeContrast.DARK;
	}
	
	throw new Error(`Unable to obtain ECommonsThemeContrast for key: ${key}`);
}

export const ECOMMONS_THEMES: ECommonsThemeContrast[] = Object.keys(ECommonsThemeContrast)
	.map((e: string): ECommonsThemeContrast => keyToECommonsThemeContrast(e));
