import { commonsTypeHasPropertyEnum } from 'tscommons-es-core';

import { ECommonsThemeColor, isECommonsThemeColor } from '../enums/ecommons-theme-color';
import { ECommonsThemeContrast, isECommonsThemeContrast } from '../enums/ecommons-theme-contrast';

export type TCommonsThemeConfig = {
		color: ECommonsThemeColor;
		contrast: ECommonsThemeContrast;
};

export function isTCommonsThemeConfig(test: unknown): test is TCommonsThemeConfig {
	if (!commonsTypeHasPropertyEnum<ECommonsThemeColor>(test, 'color', isECommonsThemeColor)) return false;
	if (!commonsTypeHasPropertyEnum<ECommonsThemeContrast>(test, 'contrast', isECommonsThemeContrast)) return false;

	return true;
}
