import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

type TLine = {
		type: string;
		content: any;
};

@Component({
	selector: 'commons-markup-inner',
	templateUrl: './commons-markup-inner.component.html',
	styleUrls: ['./commons-markup-inner.component.less']
})
export class CommonsMarkupInnerComponent extends CommonsComponent implements OnInit {
	@Input() markup!: string;
	@Input() tag?: string;
	@Input() depth!: number;
	@Input() first!: boolean;
	
	lines: TLine[] = [];
	list: string[] = [];
	
	content?: string;

	pre?: string;
	sym?: SafeHtml;
	inner?: string;
	innerTag?: string;
	post?: string;

	constructor(
			private domSanitizer: DomSanitizer
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		if (this.tag) return;
		
		const lines: string[] = this.markup
				.trim()
				.split(/[\r\n]/)
				.map((line: string): string => line.trim())
				.filter((line: string): boolean => line !== '');
		
		if (lines.length > 1) {
			this.lines = [];
			
			while (lines.length) {
				const line: string|undefined = lines.shift();
				if (line === undefined) continue;
				
				if (line.substring(0, 1) === '*') {
					const list: string[] = [ line.substring(1).trim() ];
					while (lines.length) {
						const l: string|undefined = lines.shift();
						if (l === undefined) continue;
						
						if (l.substring(0, 1) !== '*') {
							lines.unshift(l);
							break;
						}
						list.push(l.substring(1).trim());
					}
					
					this.lines.push({
							type: 'ul',
							content: list
					});
				}
				else if (line.substring(0, 1) === '#') {
					const list: string[] = [ line.substring(1).trim() ];
					while (lines.length) {
						const l: string|undefined = lines.shift();
						if (l === undefined) continue;
						
						if (l.substring(0, 1) !== '#') {
							lines.unshift(l);
							break;
						}
						list.push(l.substring(1).trim());
					}
					
					this.lines.push({
							type: 'ol',
							content: list
					});
				}
				else {
					this.lines.push({
							type: 'markup',
							content: line
					});
				}
			}
		} else {
			const line: string = lines[0];
			
			if (line !== '') {
				const breakdown: string[]|null = this.markup.match(/^(.*)(\[(h4|b|i)\](.*?)\[\/\3\])(.*)$/);
				if (breakdown === null) {
					const symbols: string[]|null = this.markup.match(/^(.*)((?:---))(.*)$/);
					if (symbols === null) {
						this.content = this.markup;
					} else {
						this.pre = symbols[1];
						switch (symbols[2]) {
							case '---':
								this.sym = this.domSanitizer.bypassSecurityTrustHtml('&mdash;');
								break;
						}
						this.post = symbols[3];
					}
				} else {
					this.pre = breakdown[1];
					this.innerTag = breakdown[3];
					this.inner = breakdown[4];
					this.post = breakdown[5];
				}
			}
		}
	}
}
