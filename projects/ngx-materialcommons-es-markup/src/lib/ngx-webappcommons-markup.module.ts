import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsMarkupComponent } from './components/commons-markup/commons-markup.component';
import { CommonsMarkupInnerComponent } from './components/commons-markup-inner/commons-markup-inner.component';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: [
				CommonsMarkupComponent,
				CommonsMarkupInnerComponent
		],
		exports: [
				CommonsMarkupComponent
		]
})
export class NgxMaterialCommonsEsMarkupModule { }
