import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsFormModule } from 'ngx-materialcommons-es-form';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { CommonsThemeSelectComponent } from './components/commons-theme-select/commons-theme-select.component';
import { CommonsFigureTallySummaryComponent } from './components/commons-figure-tally-summary/commons-figure-tally-summary.component';
import { CommonsFigureTallyBarComponent } from './components/commons-figure-tally-bar/commons-figure-tally-bar.component';
import { CommonsWordCloudComponent } from './components/commons-word-cloud/commons-word-cloud.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsPipeModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsAppModule,
				NgxMaterialCommonsEsFormModule
		],
		declarations: [
				PageNotFoundComponent,
				CommonsThemeSelectComponent,
				CommonsFigureTallySummaryComponent,
				CommonsFigureTallyBarComponent,
				CommonsWordCloudComponent
		],
		exports: [
				CommonsThemeSelectComponent,
				CommonsFigureTallySummaryComponent,
				CommonsFigureTallyBarComponent,
				CommonsWordCloudComponent
		]
})
export class NgxMaterialCommonsEsStockModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsStockModule> {
		return {
				ngModule: NgxMaterialCommonsEsStockModule,
				providers: [
				]
		};
	}
}
