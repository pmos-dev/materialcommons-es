import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, OnDestroy, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';

import { Subscription } from 'rxjs';

import { TXy } from 'tscommons-es-graphics';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

export type TWordTallyPair = {
		word: string;
		tally: number;
};

export enum ECloudShape {
		RECTANGULAR = 'rectangular',
		CIRCULAR = 'circular'
}

type TCalcPair = {
		word: string;
		tally: number;	// unused
		fontSize: number;
		rect?: DOMRectReadOnly;
};

type TWidthHeight = {
		width: number;
		height: number;
};

type TXywh = TXy & TWidthHeight;

type TDisplayPair = {
		word: string;
		tally: number;	// unused
		fontSize: number;
		position: TXywh;
};

function calculateRect(element: ElementRef<HTMLElement>): Promise<DOMRectReadOnly> {
	return new Promise<DOMRectReadOnly>((resolve: (result: DOMRectReadOnly) => void, reject: (e: Error) => void): void => {
		const resizeObserver = new ResizeObserver((entries: ResizeObserverEntry[]) => {
			if (entries.length !== 1) {
				reject(new Error('Resize observer was not a single entry. This should not be possible'));
			}

			const entry: ResizeObserverEntry = entries[0];
			const cr: DOMRectReadOnly = entry.contentRect;
			resolve(cr);

			resizeObserver.disconnect();
		});
		resizeObserver.observe(element.nativeElement);
	});
}

@Component({
		selector: 'commons-word-cloud',
		templateUrl: './commons-word-cloud.component.html',
		styleUrls: ['./commons-word-cloud.component.less']
})
export class CommonsWordCloudComponent extends CommonsComponent implements AfterViewInit, OnChanges, OnDestroy {
	@ViewChildren('sizecalc') sizecalcs: QueryList<ElementRef<HTMLElement>>|undefined;

	@ViewChild('container') container: ElementRef<HTMLElement>|undefined;
	private containerObserver: ResizeObserver|undefined;
	private containerSize: TWidthHeight|undefined;

	@Input() words: TWordTallyPair[] = [];
	@Input() limit: number = 100;
	@Input() shape: ECloudShape = ECloudShape.CIRCULAR;
	@Input() maxFontSizeHeightRatio: number = 0.1;
	@Input() padding: number = 0.1;

	calcPairs: TCalcPair[] = [];
	displayPairs: TDisplayPair[] = [];

	private recalculating: Subscription|undefined;

	constructor(
			private changeDetector: ChangeDetectorRef
	) {
		super();
	}

	override ngAfterViewInit(): void {
		super.ngAfterViewInit();

		if (!this.container || !this.container.nativeElement) return;

		this.containerSize = {
				width: this.container.nativeElement.clientWidth,
				height: this.container.nativeElement.clientHeight
		};

		this.containerObserver = new ResizeObserver((entries: ResizeObserverEntry[]): void => {
			if (entries.length < 1) return;

			this.containerSize = {
					width: entries[0].contentRect.width,
					height: entries[0].contentRect.height
			};
			this.recalculate();
		});
		this.containerObserver.observe(this.container.nativeElement);
	}

	override ngOnDestroy(): void {
		if (this.containerObserver) {
			this.containerObserver.unobserve(this.container!.nativeElement);
			this.containerObserver = undefined;
		}

		super.ngOnDestroy();
	}

	ngOnChanges(_changes: SimpleChanges): void {
		if (!this.containerSize) return;

		this.recalculate();
	}

	private recalculate(): void {
		if (this.recalculating) this.recalculating.unsubscribe();

		this.calcPairs = [];
		this.displayPairs = [];

		const max: number = Math.max(1, ...this.words.map((pair: TWordTallyPair): number => pair.tally));

		const pairs: TCalcPair[] = this.words
				.map((pair: TWordTallyPair): TCalcPair => ({
						word: pair.word,
						tally: pair.tally || 0,
						fontSize: ((pair.tally || 0) / max) * (this.containerSize!.height * this.maxFontSizeHeightRatio)
				}))
				.sort((a: TCalcPair, b: TCalcPair): number => {
					if (a.tally < b.tally) return -1;
					if (a.tally > b.tally) return 1;
					return 0;
				})
				.reverse();

		this.calcPairs = pairs.slice(0, this.limit);

		this.recalculating = this.setTimeout(
				10,
				async (): Promise<void> => {
					if (!this.sizecalcs) return;

					const rectPromises: Promise<DOMRectReadOnly>[] = this.sizecalcs.toArray()
							.map((entry: ElementRef<HTMLElement>): Promise<DOMRectReadOnly> => calculateRect(entry));
					const rects: DOMRectReadOnly[] = await Promise.all(rectPromises);

					if (rects.length !== this.calcPairs.length) throw new Error('Pairs/sizes mismatch');
					for (let i = rects.length; i-- > 0;) pairs[i].rect = rects[i];

					this.render();

					this.recalculating = undefined;
				}
		);
	}

	private render(): void {
		if (!this.containerSize) return;

		const ratio: number = this.containerSize.width / this.containerSize.height;

		const allocated: TDisplayPair[] = [];

		const overlaps: (
				a: TXywh,
				b: TXywh
		) => boolean = (
				a: TXywh,
				b: TXywh
		): boolean => {
			if ((a.x + a.width) < b.x) return false;
			if ((a.y + a.height) < b.y) return false;

			if (a.x > (b.x + b.width)) return false;
			if (a.y > (b.y + b.height)) return false;

			return true;
		};

		const overlapsAny: (position: TXywh) => boolean = (position: TXywh): boolean => {
			for (const existing of allocated) {
				if (overlaps(position, existing.position)) return true;
			}

			return false;
		};

		for (const pair of this.calcPairs) {
			let i: number = -1;

			const cx: number = this.containerSize!.width / 2;
			const cy: number = this.containerSize!.height / 2;

			const attempt: TXywh = {
					x: cx,
					y: cy,
					width: pair.rect!.width + (pair.rect!.width * this.padding),
					height: pair.rect!.height + (pair.rect!.width * this.padding)
			};

			let ttl: number = 100000;
			const direction: number = Math.random() >= 0.5 ? 1 : -1;
			while (true) {
				i += direction;
				if (--ttl < 0) throw new Error('TTL timeout');

				attempt.x = cx + (Math.sin(2 * Math.PI * i * 0.01) * i * 0.02 * ratio);
				attempt.y = cy + (Math.cos(2 * Math.PI * i * (this.shape === ECloudShape.CIRCULAR ? 0.01 : 0.021)) * i * 0.02);
				attempt.x -= attempt.width / 2;
				attempt.y -= attempt.height / 2;

				if (overlapsAny(attempt)) continue;
				break;
			}

			allocated.push({
					word: pair.word,
					tally: pair.tally,
					fontSize: pair.fontSize,
					position: attempt
			});
		}
		this.displayPairs = allocated;
		this.changeDetector.detectChanges();
	}
}
