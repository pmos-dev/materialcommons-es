import { Component, AfterViewInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';
import { CommonsLoadingService } from 'ngx-angularcommons-es-app';

@Component({
		templateUrl: './page-not-found.component.html',
		styleUrls: ['./page-not-found.component.less']
})
export class PageNotFoundComponent extends CommonsComponent implements AfterViewInit {

	constructor(
			private loadingService: CommonsLoadingService
	) {
		super();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();
		
		this.loadingService.loaded();
	}

}
