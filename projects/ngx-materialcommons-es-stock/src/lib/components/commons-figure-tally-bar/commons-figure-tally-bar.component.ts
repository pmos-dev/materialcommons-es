import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, ViewChild } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

const MIN_ITEM_WIDTH: number = 75;

@Component({
		selector: 'commons-figure-tally-bar',
		templateUrl: './commons-figure-tally-bar.component.html',
		styleUrls: ['./commons-figure-tally-bar.component.less']
})
export class CommonsFigureTallyBarComponent extends CommonsComponent implements AfterViewInit {
	@ViewChild('containerelement') containerElement: ElementRef<HTMLElement>|undefined;

	@Input() labels: string[] = [];
	@Input() tallies: Map<string, number|undefined> = new Map<string, number|undefined>();

	private maxColumns: number|undefined;

	constructor(
			private changeDetector: ChangeDetectorRef
	) {
		super();
	}

	get possibleLabels(): string[] {
		if (!this.maxColumns) return [ ...this.labels ];

		return this.labels.slice(0, this.maxColumns);
	}

	get columns(): string {
		return `repeat(${this.possibleLabels.length}, 1fr)`;
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();

		if (!this.containerElement) return;

		const resizeObserver = new ResizeObserver((entries: ResizeObserverEntry[]) => {
			if (entries.length !== 1) throw new Error('Resize observer was not a single entry. This should not be possible');
			const entry: ResizeObserverEntry = entries[0];

			if (this.labels.length === 0) return;
		
			const cr: DOMRectReadOnly = entry.contentRect;

			this.maxColumns = this.labels.length;

			let ttl: number = 100;
			while (cr.width < (this.maxColumns * (MIN_ITEM_WIDTH + 4))) {
				if (ttl-- < 0) break;
				if (this.maxColumns <= 1) break;

				this.maxColumns--;
			}

			// ResizeObserver is not patched for ChangeDetection
			this.changeDetector.detectChanges();
		});
		resizeObserver.observe(this.containerElement.nativeElement);
	}
}
