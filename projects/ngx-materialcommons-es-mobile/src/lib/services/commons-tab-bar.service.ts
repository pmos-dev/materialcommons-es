import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class CommonsTabBarService {
	private showEmitter: EventEmitter<string> = new EventEmitter<string>(true);
	private hideEmitter: EventEmitter<string> = new EventEmitter<string>(true);

	public show(name: string): void {
		this.showEmitter.emit(name);
	}

	public hide(name: string): void {
		this.hideEmitter.emit(name);
	}
	
	public showObservable(): Observable<string> {
		return this.showEmitter;
	}
	
	public hideObservable(): Observable<string> {
		return this.hideEmitter;
	}
}
