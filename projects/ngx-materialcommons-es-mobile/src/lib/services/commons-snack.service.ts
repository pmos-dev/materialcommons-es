import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { ICommonsSnack } from 'ngx-materialcommons-es-app';
import { ECommonsSnackType } from 'ngx-materialcommons-es-app';

@Injectable()
export class CommonsSnackService {
	public static generate(type: ECommonsSnackType, message: string): ICommonsSnack {
		return { type: type, message: message, timestamp: Date.now() };
	}

	private subject: Subject<ICommonsSnack> = new Subject<ICommonsSnack>();

	public observable(): Observable<ICommonsSnack> {
		return this.subject.asObservable();
	}

	public notification(message: string): void {
		this.subject.next(CommonsSnackService.generate(ECommonsSnackType.NOTIFICATION, message));
	}

	public error(message: string): void {
		this.subject.next(CommonsSnackService.generate(ECommonsSnackType.ERROR, message));
	}

	public success(message: string): void {
		this.subject.next(CommonsSnackService.generate(ECommonsSnackType.SUCCESS, message));
	}

}
