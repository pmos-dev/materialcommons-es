import { EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

export interface ICommonsUiParentChildComponentServiceItem<T> {
		component: T;
		name?: string;
		visible: boolean;
}

export abstract class CommonsUiParentChildComponentService<T, I extends ICommonsUiParentChildComponentServiceItem<T>> {
	private onItemCountChanged: EventEmitter<number> = new EventEmitter<number>(true);
	
	private items: I[] = [];

	public itemCountChangedObservable(): Observable<number> {
		return this.onItemCountChanged;
	}
	
	protected getExistingByComponent(component: T): I|undefined {
		return this.items
			.find((item: I): boolean => item.component === component);
	}
	
	protected getExistingByName(name: string): I|undefined {
		return this.items
			.find((item: I): boolean => item.name === name);
	}
	
	private refresh(): void {
		const length: number = this.items
			.filter((item: I): boolean => item.visible)
			.length;

		this.onItemCountChanged.emit(length);
	}
	
	public getComponentByName(name: string): T|undefined {
		const item: I|undefined = this.getExistingByName(name);
		if (!item) return undefined;
		
		return item.component;
	}
	
	public getNameByComponent(component: T): string|undefined {
		const item: I|undefined = this.getExistingByComponent(component);
		if (!item) return undefined;
		
		return item.name;
	}
	
	public addItem(component: T, name?: string, visible = true): void {
		const existing: I|undefined = this.getExistingByComponent(component);
		
		if (existing) {
			// replace
			existing.component = component;
		} else {
			// append
			const item: I = this.buildItem(component, name, visible);
			
			this.items.push(item);
			
			this.refresh();
		}
	}
	
	protected abstract buildItem(component: T, name: string|undefined, visible: boolean): I;
	
	public removeItem(component: T): void {
		const existing: I|undefined = this.getExistingByComponent(component);
		if (!existing) return;

		this.items = this.items
			.filter((item: I): boolean => item.component !== component);
		
		this.refresh();
	}
	
	public setVisible(component: T, visible: boolean): boolean {
		const existing: I|undefined = this.getExistingByComponent(component);
		if (!existing) return false;
		
		existing.visible = visible;
		this.refresh();
		
		return true;
	}
	
	public show(component: T): boolean {
		return this.setVisible(component, true);
	}
	
	public hide(component: T): boolean {
		return this.setVisible(component, false);
	}
	
	public setVisibleByName(name: string, visible: boolean): boolean {
		const existing: I|undefined = this.getExistingByName(name);
		if (!existing) return false;
		
		existing.visible = visible;
		this.refresh();
		
		return true;
	}
	
	public showByName(name: string): boolean {
		return this.setVisibleByName(name, true);
	}
	
	public hideByName(name: string): boolean {
		return this.setVisibleByName(name, false);
	}
}
