import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-switch',
		templateUrl: './commons-switch.component.html',
		styleUrls: ['./commons-switch.component.less']
})
export class CommonsSwitchComponent extends CommonsComponent {
	@Input() on: boolean = false;
	// eslint-disable-next-line @angular-eslint/no-output-on-prefix
	@Output() onChange: EventEmitter<boolean> = new EventEmitter<boolean>();
	
	@Input() disabled: boolean = false;
	
	panning: boolean = false;
	pan: boolean = false;
	active: boolean = false;

	doToggle(): void {
		if (this.disabled) return;
		
		if (this.on) this.onChange.emit(false);
		else this.onChange.emit(true);
	}
	
	doActive(state: boolean): void {
		this.active = state;
	}
	
	doPanStart(): void {
		if (this.disabled) return;
		
		this.panning = true;
		this.pan = this.on;
	}
	
	doPanEnd(): void {
		if (this.disabled) return;
		
		this.panning = false;
		
		if (this.pan !== this.on) this.doToggle();
	}
	
	doPanMove(event: any): void {
		if (this.disabled) return;
		
		if (event.direction === 2 && event.deltaX < -10) this.pan = false;
		if (event.direction === 4 && event.deltaX > 10) this.pan = true;
	}
}
