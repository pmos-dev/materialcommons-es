import { Component, OnInit, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsTabBarService } from '../../services/commons-tab-bar.service';

@Component({
		selector: 'commons-tab-bar',
		templateUrl: './commons-tab-bar.component.html',
		styleUrls: ['./commons-tab-bar.component.less']
})
export class CommonsTabBarComponent extends CommonsComponent implements OnInit {
	@Input() name!: string;

	show: boolean = true;
	
	constructor(
			private tabBarService: CommonsTabBarService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.show = true;

		this.subscribe(
				this.tabBarService.showObservable(),
				(name: string): void => {
					if (name === this.name) this.show = true;
				}
		);
		
		this.subscribe(
				this.tabBarService.hideObservable(),
				(name: string): void => {
					if (name === this.name) this.show = false;
				}
		);
	}

}
