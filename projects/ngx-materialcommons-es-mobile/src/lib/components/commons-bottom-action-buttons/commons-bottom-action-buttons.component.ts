import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { EMaterialDesignIcons } from 'ngx-materialcommons-es-core';

import { ICommonsAction } from '../../interfaces/icommons-action';

@Component({
		selector: 'commons-bottom-action-buttons',
		templateUrl: './commons-bottom-action-buttons.component.html',
		styleUrls: ['./commons-bottom-action-buttons.component.less']
})
export class CommonsBottomActionButtonsComponent extends CommonsComponent {
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	@Input() actions: ICommonsAction[] = [];
	
	@Output() action: EventEmitter<string> = new EventEmitter<string>();
	
	list(main: boolean): ICommonsAction[] {
		return this.actions
			.filter((a: ICommonsAction): boolean => a.main === main)
			.map((a: ICommonsAction): ICommonsAction => {
				a.iconSet = a.iconSet || EMaterialDesignIcons.MATERIALDESIGN;
				return a;
			});
	}
	
	doAction(action: string): void {
		this.action.emit(action);
	}
}
