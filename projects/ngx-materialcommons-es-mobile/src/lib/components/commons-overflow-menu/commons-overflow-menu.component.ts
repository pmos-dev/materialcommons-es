import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsMenuService } from 'ngx-materialcommons-es-app';

import { CommonsOverflowService } from '../../services/commons-overflow.service';

import { ICommonsOverflowItem } from '../../interfaces/icommons-overflow-item';

@Component({
		selector: 'commons-overflow-menu',
		templateUrl: './commons-overflow-menu.component.html',
		styleUrls: ['./commons-overflow-menu.component.less']
})
export class CommonsOverflowMenuComponent extends CommonsComponent implements OnInit {
	public static divider(): ICommonsOverflowItem {
		return { name: '_divider', caption: '', divider: true };
	}

	menuVisible: boolean = false;
	items: ICommonsOverflowItem[] = [];

	constructor(
			private overflowService: CommonsOverflowService,
			private menuService: CommonsMenuService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.overflowService.showObservable(),
				(): void => {
					this.menuVisible = true;
				}
		);
		
		this.subscribe(
				this.menuService.hideObservable(),
				(name: string): void => {
					if (name === '_overflow') this.menuVisible = false;
				}
		);
		
		this.subscribe(
				this.overflowService.setObservable(),
				(items: ICommonsOverflowItem[]): void => {
					this.items = items;
				}
		);
	}

	doSelect(item: any): void {
		if (item.disabled) return;
		
		this.overflowService.select(item);
		this.overflowService.close();
	}
}
