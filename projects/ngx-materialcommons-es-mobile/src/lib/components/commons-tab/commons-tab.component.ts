import { Component, HostBinding, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-tab]',	// a tags so we can use routerLink
		templateUrl: './commons-tab.component.html',
		styleUrls: ['./commons-tab.component.less']
})
export class CommonsTabComponent extends CommonsComponent {
	@HostBinding('attr.disabled') @Input() disabled: boolean|undefined = undefined;

	@Input() label?: string;
	@Input() icon?: string;
}
