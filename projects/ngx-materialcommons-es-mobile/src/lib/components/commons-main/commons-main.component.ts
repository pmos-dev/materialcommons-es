import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-main',
		templateUrl: './commons-main.component.html',
		styleUrls: ['./commons-main.component.less']
})
export class CommonsMainComponent extends CommonsComponent {
}
