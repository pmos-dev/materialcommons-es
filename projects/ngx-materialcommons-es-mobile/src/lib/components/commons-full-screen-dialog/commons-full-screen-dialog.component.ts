import { AfterViewInit, OnDestroy } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsFullScreenService } from '../../services/commons-full-screen.service';

export abstract class CommonsFullScreenDialogComponent extends CommonsComponent implements AfterViewInit, OnDestroy {
	private tabbar: string|undefined;

	constructor(
		private fullScreenService: CommonsFullScreenService
	) {
		super();
	}

	protected open(
			title: string,
			callback: () => void,
			tabbar?: string
	): void {
		this.tabbar = tabbar;
			
		this.fullScreenService.open(title, callback, this.tabbar);
	}
	
	protected back(): void {
		this.fullScreenService.back();
	}

	protected reset(): void {
		this.fullScreenService.reset();
	}

	ngAfterViewInit(): void {
		super.ngAfterViewInit();

		// NB, this may not be necessary any more.
		window.scrollTo(0, 0);
	}

	ngOnDestroy(): void {
		if (this.tabbar) this.fullScreenService.close(this.tabbar);
		
		super.ngOnDestroy();
	}

}
