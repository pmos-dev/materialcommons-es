import { Component, OnInit, Input } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ICommonsSnack } from 'ngx-materialcommons-es-app';

import { CommonsSnackService } from '../../services/commons-snack.service';

@Component({
		selector: 'commons-snackbar',
		templateUrl: './commons-snackbar.component.html',
		styleUrls: ['./commons-snackbar.component.less'],
		animations: [
				trigger(
						'closeAnimation',
						[
								transition('* => void', [
										style({ opacity: 1 }),
										animate('100ms', style({ opacity: 0 }))
								])
						]
				)
		]
})
export class CommonsSnackbarComponent extends CommonsComponent implements OnInit {
	@Input() timeout?: number;
	@Input() allowIgnore: boolean = true;
	@Input() maxSnacks: number = 5;
	
	snacks: ICommonsSnack[] = [];
	
	constructor(
			private snackService: CommonsSnackService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		if (this.maxSnacks > 10) this.maxSnacks = 10;

		this.subscribe(
				this.snackService.observable(),
				(snack) => this.snacks.push(snack)
		);
	}
	
	doIgnore(snack: ICommonsSnack): void {
		this.snacks.splice(this.snacks.indexOf(snack), 1);
	}
}
