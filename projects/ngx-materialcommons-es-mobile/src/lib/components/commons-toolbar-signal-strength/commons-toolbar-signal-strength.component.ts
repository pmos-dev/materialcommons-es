import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-toolbar-signal-strength',
		templateUrl: './commons-toolbar-signal-strength.component.html',
		styleUrls: ['./commons-toolbar-signal-strength.component.less']
})
export class CommonsToolbarSignalStrengthComponent extends CommonsComponent {
	@Input() strength: number = 0;
}
