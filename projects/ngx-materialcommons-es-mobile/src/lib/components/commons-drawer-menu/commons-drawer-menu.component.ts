import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsDrawerService } from '../../services/commons-drawer.service';

import { ECommonsDrawerDirection } from '../../enums/ecommons-drawer-direction';

@Component({
		selector: 'commons-drawer-menu',
		templateUrl: './commons-drawer-menu.component.html',
		styleUrls: ['./commons-drawer-menu.component.less']
})
export class CommonsDrawerMenuComponent extends CommonsComponent {
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	constructor(
			private drawerService: CommonsDrawerService
	) {
		super();
	}

	doClose(): void {
		this.drawerService.hide('_drawermenu');
	}
	
	doSwipe(event: HammerInput): void {
		if (event.direction === 2) this.doClose();
	}
}
