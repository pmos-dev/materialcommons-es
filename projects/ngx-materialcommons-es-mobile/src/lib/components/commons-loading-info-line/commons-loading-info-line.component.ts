import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-loading-info-line',
		templateUrl: './commons-loading-info-line.component.html',
		styleUrls: ['./commons-loading-info-line.component.less']
})
export class CommonsLoadingInfoLineComponent extends CommonsComponent {
	@Input() rainbow: boolean = false;
}
