import { Component, Input, HostBinding, HostListener } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsUpService } from 'ngx-materialcommons-es-app';
import { EMaterialDesignIcons } from 'ngx-materialcommons-es-core';

@Component({
		selector: '[commons-bottom-navigation-bar-item]',
		templateUrl: './commons-bottom-navigation-bar-item.component.html',
		styleUrls: ['./commons-bottom-navigation-bar-item.component.less']
})
export class CommonsBottomNavigationBarItemComponent extends CommonsComponent {
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	@HostBinding('attr.disabled') @Input() disabled: boolean|undefined = undefined;
	
	@Input() icon: string|undefined;
	@Input() iconSet: string = EMaterialDesignIcons.MATERIALDESIGN;
	@Input() caption: string|undefined;
	@Input() badge?: string;
	
	@HostListener('click') resetUp(): void {
		this.upService.reset();
	}

	constructor(private upService: CommonsUpService) {
		super();
	}
}
