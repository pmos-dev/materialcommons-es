import { Component, OnInit, Input } from '@angular/core';
import { trigger, style, animate, transition, state } from '@angular/animations';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsDrawerService } from '../../services/commons-drawer.service';

import { ECommonsDrawerDirection } from '../../enums/ecommons-drawer-direction';

@Component({
		selector: 'commons-drawer',
		templateUrl: './commons-drawer.component.html',
		styleUrls: ['./commons-drawer.component.less'],
		animations: [
				trigger('drawerBackgroundFadeInOutAnimation', [
						transition('void => *', [
								style({ opacity: 0 }),
								animate('200ms', style({ opacity: 1 }))
						]),
						transition('* => void', [
								style({ opacity: 1 }),
								animate('200ms', style({ opacity: 0 }))
						])
				]),
				trigger('drawerSlideInOutAnimationBottom', [
						state('inactive', style({
								bottom: '-100vh'
						})),
						state('active', style({
								bottom: 0
						})),
						state('na', style({
						})),
						transition('inactive => active', animate('200ms ease-in')),
						transition('active => inactive', animate('200ms ease-out'))
				]),
				trigger('drawerSlideInOutAnimationLeft', [
						state('inactive', style({
								left: '-100vw'
						})),
						state('active', style({
								left: 0
						})),
						state('na', style({
						})),
						transition('inactive => active', animate('150ms ease-in')),
						transition('active => inactive', animate('150ms ease-out'))
				]),
				trigger('drawerSlideInOutAnimationTop', [
						state('inactive', style({
								top: '-100vh'
						})),
						state('active', style({
								top: 0
						})),
						state('na', style({
						})),
						transition('inactive => active', animate('200ms ease-in')),
						transition('active => inactive', animate('200ms ease-out'))
				]),
				trigger('drawerSlideInOutAnimationRight', [
						state('inactive', style({
								right: '-100vw'
						})),
						state('active', style({
								right: 0
						})),
						state('na', style({
						})),
						transition('inactive => active', animate('150ms ease-in')),
						transition('active => inactive', animate('150ms ease-out'))
				])
		]
})
export class CommonsDrawerComponent extends CommonsComponent implements OnInit {
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	@Input() name!: string;
	@Input() direction!: ECommonsDrawerDirection;
	
	leftState: string = 'na';
	bottomState: string = 'na';
	rightState: string = 'na';
	topState: string = 'na';
	
	constructor(
			private drawerService: CommonsDrawerService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		switch (this.direction) {
		case ECommonsDrawerDirection.BOTTOM:
			this.bottomState = 'inactive';
			break;
		case ECommonsDrawerDirection.LEFT:
			this.leftState = 'inactive';
			break;
		case ECommonsDrawerDirection.RIGHT:
			this.rightState = 'inactive';
			break;
		case ECommonsDrawerDirection.TOP:
			this.topState = 'inactive';
			break;
		}

		this.subscribe(
				this.drawerService.showObservable(),
				(name: string): void => {
					if (name === this.name) {
						switch (this.direction) {
						case ECommonsDrawerDirection.BOTTOM:
							this.bottomState = 'active';
							break;
						case ECommonsDrawerDirection.LEFT:
							this.leftState = 'active';
							break;
						case ECommonsDrawerDirection.RIGHT:
							this.rightState = 'active';
							break;
						case ECommonsDrawerDirection.TOP:
							this.topState = 'active';
							break;
						}
					}
				}
		);
		
		this.subscribe(
				this.drawerService.hideObservable(),
				(name: string): void => {
					if (name === this.name) {
						switch (this.direction) {
						case ECommonsDrawerDirection.BOTTOM:
							this.bottomState = 'inactive';
							break;
						case ECommonsDrawerDirection.LEFT:
							this.leftState = 'inactive';
							break;
						case ECommonsDrawerDirection.RIGHT:
							this.rightState = 'inactive';
							break;
						case ECommonsDrawerDirection.TOP:
							this.topState = 'inactive';
							break;
						}
					}
				}
		);
	}

	doClose(): void {
		this.drawerService.hide(this.name);
		this.drawerService.cancelled(this.name);
	}
	
	doSwipe(event: any): void {
		if (this.direction === ECommonsDrawerDirection.LEFT && event.direction === 2) this.doClose();
		if (this.direction === ECommonsDrawerDirection.BOTTOM && event.direction === 16) this.doClose();
		if (this.direction === ECommonsDrawerDirection.RIGHT && event.direction === 4) this.doClose();
		if (this.direction === ECommonsDrawerDirection.TOP && event.direction === 8) this.doClose();
	}
}
