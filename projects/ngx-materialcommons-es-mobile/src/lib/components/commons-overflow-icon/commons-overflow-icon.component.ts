import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsOverflowService } from '../../services/commons-overflow.service';

import { ICommonsOverflowItem } from '../../interfaces/icommons-overflow-item';

@Component({
		selector: 'commons-overflow-icon',
		templateUrl: './commons-overflow-icon.component.html',
		styleUrls: ['./commons-overflow-icon.component.less']
})
export class CommonsOverflowIconComponent extends CommonsComponent implements OnInit {
	hasOverflow: boolean = false;

	constructor(
			private overflowService: CommonsOverflowService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.overflowService.setObservable(),
				(items: ICommonsOverflowItem[]): void => {
					this.hasOverflow = items.length > 0;
				}
		);
	}

	doShowOverflow(): void {
		this.overflowService.open();
	}

}
