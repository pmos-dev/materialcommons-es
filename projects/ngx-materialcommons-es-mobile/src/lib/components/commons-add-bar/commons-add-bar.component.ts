import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-add-bar',
		templateUrl: './commons-add-bar.component.html',
		styleUrls: ['./commons-add-bar.component.less']
})
export class CommonsAddBarComponent extends CommonsComponent {
	@Input() disabled: boolean = false;
	
	@Output() add: EventEmitter<void> = new EventEmitter<void>();

	doAdd(): void {
		if (this.disabled) return;
		this.add.emit();
	}

}
