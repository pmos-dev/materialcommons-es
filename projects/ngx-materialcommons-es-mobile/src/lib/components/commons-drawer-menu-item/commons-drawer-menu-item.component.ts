import { Component, Input, HostListener } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsDrawerService } from '../../services/commons-drawer.service';

@Component({
		selector: '[commons-drawer-menu-item]',
		templateUrl: './commons-drawer-menu-item.component.html',
		styleUrls: ['./commons-drawer-menu-item.component.less']
})
export class CommonsDrawerMenuItemComponent extends CommonsComponent {
	@Input() disabled: boolean = false;
	@Input() autoClose: boolean = true;
	@Input() icon!: string;

	@HostListener('click') doClick(): void {
		if (!this.disabled && this.autoClose) this.doClose();
	}

	constructor(
		private drawerService: CommonsDrawerService
	) {
		super();
	}

	public doClose(): void {
		this.drawerService.hide('_drawermenu');
	}
}
