import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-bottom-navigation-bar',
		templateUrl: './commons-bottom-navigation-bar.component.html',
		styleUrls: ['./commons-bottom-navigation-bar.component.less']
})
export class CommonsBottomNavigationBarComponent extends CommonsComponent {}
