import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';
import { CommonsStatusService } from 'ngx-angularcommons-es-core';

import { CommonsToolbarStatusService } from '../../services/commons-toolbar-status.service';

@Component({
		selector: '[commons-toolbar-status]',
		templateUrl: './commons-toolbar-status.component.html',
		styleUrls: ['./commons-toolbar-status.component.less']
})
export class CommonsToolbarStatusComponent extends CommonsComponent implements OnInit, OnDestroy {
	@Input() name!: string;
	@Input() default!: boolean;
	
	state: boolean = false;

	constructor(
			private statusService: CommonsStatusService,
			private toolbarStatusService: CommonsToolbarStatusService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.toolbarStatusService.addItem(this, this.name, this.state);
		
		this.state = this.default;
		
		this.subscribe(
				this.statusService.observable(this.name, this.default),
				(state: boolean): void => {
					this.state = state;
					this.toolbarStatusService.setVisible(this, state);
				}
		);
	}

	ngOnDestroy(): void {
		this.toolbarStatusService.removeItem(this);

		super.ngOnDestroy();
	}

}
