import { Injectable, NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsGraphicsModule } from 'ngx-materialcommons-es-graphics';

import { CommonsMainComponent } from './components/commons-main/commons-main.component';
import { CommonsToolbarComponent } from './components/commons-toolbar/commons-toolbar.component';
import { CommonsBottomNavigationBarComponent } from './components/commons-bottom-navigation-bar/commons-bottom-navigation-bar.component';
import { CommonsBottomNavigationBarItemComponent } from './components/commons-bottom-navigation-bar-item/commons-bottom-navigation-bar-item.component';
import { CommonsDrawerMenuComponent } from './components/commons-drawer-menu/commons-drawer-menu.component';
import { CommonsAddBarComponent } from './components/commons-add-bar/commons-add-bar.component';
import { CommonsTabBarComponent } from './components/commons-tab-bar/commons-tab-bar.component';
import { CommonsTabComponent } from './components/commons-tab/commons-tab.component';
import { CommonsOverflowIconComponent } from './components/commons-overflow-icon/commons-overflow-icon.component';
import { CommonsOverflowMenuComponent } from './components/commons-overflow-menu/commons-overflow-menu.component';
import { CommonsSnackbarComponent } from './components/commons-snackbar/commons-snackbar.component';
import { CommonsDrawerMenuItemComponent } from './components/commons-drawer-menu-item/commons-drawer-menu-item.component';
import { CommonsSplashComponent } from './components/commons-splash/commons-splash.component';
import { CommonsSwitchComponent } from './components/commons-switch/commons-switch.component';
import { CommonsDrawerComponent } from './components/commons-drawer/commons-drawer.component';
import { CommonsToolbarStatusComponent } from './components/commons-toolbar-status/commons-toolbar-status.component';
import { CommonsActionBarComponent } from './components/commons-action-bar/commons-action-bar.component';
import { CommonsBottomActionButtonsComponent } from './components/commons-bottom-action-buttons/commons-bottom-action-buttons.component';
import { CommonsCardComponent } from './components/commons-card/commons-card.component';
import { CommonsLoadingInfoLineComponent } from './components/commons-loading-info-line/commons-loading-info-line.component';
import { CommonsToolbarSignalStrengthComponent } from './components/commons-toolbar-signal-strength/commons-toolbar-signal-strength.component';

import { CommonsTabBarService } from './services/commons-tab-bar.service';
import { CommonsOverflowService } from './services/commons-overflow.service';
import { CommonsSnackService } from './services/commons-snack.service';
import { CommonsDrawerService } from './services/commons-drawer.service';
import { CommonsFullScreenService } from './services/commons-full-screen.service';
import { CommonsToolbarStatusService } from './services/commons-toolbar-status.service';

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
		overrides = {
				// override hammerjs default configuration
				swipe: {
						direction: 31 // /! ugly hack to allow swipe in all direction
				},
				pan: {
						direction: 30 // /! ugly hack to allow pan in all direction
				},
				press: {
						time: 600
				}
		} as any;
}

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsCoreModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsAppModule,
				NgxMaterialCommonsEsGraphicsModule
		],
		declarations: [
				CommonsMainComponent,
				CommonsToolbarComponent,
				CommonsToolbarStatusComponent,
				CommonsBottomNavigationBarComponent,
				CommonsBottomNavigationBarItemComponent,
				CommonsDrawerMenuComponent,
				CommonsDrawerMenuItemComponent,
				CommonsAddBarComponent,
				CommonsTabBarComponent,
				CommonsTabComponent,
				CommonsOverflowIconComponent,
				CommonsOverflowMenuComponent,
				CommonsSnackbarComponent,
				CommonsSplashComponent,
				CommonsSwitchComponent,
				CommonsDrawerComponent,
				CommonsActionBarComponent,
				CommonsBottomActionButtonsComponent,
				CommonsCardComponent,
				CommonsLoadingInfoLineComponent,
				CommonsToolbarSignalStrengthComponent
		],
		exports: [
				CommonsMainComponent,
				CommonsToolbarComponent,
				CommonsToolbarStatusComponent,
				CommonsBottomNavigationBarComponent,
				CommonsBottomNavigationBarItemComponent,
				CommonsDrawerMenuComponent,
				CommonsDrawerMenuItemComponent,
				CommonsAddBarComponent,
				CommonsTabBarComponent,
				CommonsTabComponent,
				CommonsOverflowMenuComponent,
				CommonsSnackbarComponent,
				CommonsSplashComponent,
				CommonsSwitchComponent,
				CommonsDrawerComponent,
				CommonsActionBarComponent,
				CommonsBottomActionButtonsComponent,
				CommonsCardComponent,
				CommonsLoadingInfoLineComponent,
				CommonsToolbarSignalStrengthComponent
		]
})
export class NgxMaterialCommonsEsMobileModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsMobileModule> {
		return {
				ngModule: NgxMaterialCommonsEsMobileModule,
				providers: [
						CommonsTabBarService,
						CommonsOverflowService,
						CommonsSnackService,
						CommonsDrawerService,
						CommonsFullScreenService,
						CommonsToolbarStatusService,
						{
								provide: HAMMER_GESTURE_CONFIG,
								useClass: MyHammerConfig
						}
				]
		};
	}
}
