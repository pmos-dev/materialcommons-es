export interface ICommonsOverflowItem {
	name: string;
	caption: string;
	icon?: string;
	disabled?: boolean;
	divider?: boolean;
}
