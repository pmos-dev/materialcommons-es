export interface ICommonsAction {
	action: string;
	icon: string;
	iconSet?: string;
	main: boolean;
	disabled?: boolean;
}
