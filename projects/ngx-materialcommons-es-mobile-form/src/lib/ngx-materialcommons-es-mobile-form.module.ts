import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';

import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsFormModule } from 'ngx-materialcommons-es-form';
import { NgxMaterialCommonsEsMobileModule } from 'ngx-materialcommons-es-mobile';

import { CommonsDrawerDropdownComponent } from './components/commons-drawer-dropdown/commons-drawer-dropdown.component';
import { CommonsDrawerSelectorComponent } from './components/commons-drawer-selector/commons-drawer-selector.component';
import { CommonsCylinderPickerComponent } from './components/commons-cylinder-picker/commons-cylinder-picker.component';
import { CommonsCylinderPickerTimeComponent } from './components/commons-cylinder-picker-time/commons-cylinder-picker-time.component';
import { CommonsCylinderPickerDateComponent } from './components/commons-cylinder-picker-date/commons-cylinder-picker-date.component';
import { CommonsCircularPickerComponent } from './components/commons-circular-picker/commons-circular-picker.component';
import { CommonsDialogTimeComponent } from './components/commons-dialog-time/commons-dialog-time.component';
import { CommonsDateTimeComponent } from './components/commons-date-time/commons-date-time.component';

import { CommonsSelectorService } from './services/commons-selector.service';
import { CommonsDialogTimeService } from './services/commons-dialog-time.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsPipeModule,
				NgxMaterialCommonsEsAppModule,
				NgxMaterialCommonsEsFormModule,
				NgxMaterialCommonsEsMobileModule
		],
		declarations: [
				CommonsDrawerDropdownComponent,
				CommonsDrawerSelectorComponent,
				CommonsCylinderPickerComponent,
				CommonsCylinderPickerTimeComponent,
				CommonsCylinderPickerDateComponent,
				CommonsCircularPickerComponent,
				CommonsDialogTimeComponent,
				CommonsDateTimeComponent
		],
		exports: [
				CommonsDrawerDropdownComponent,
				CommonsDrawerSelectorComponent,
				CommonsCylinderPickerComponent,
				CommonsCylinderPickerTimeComponent,
				CommonsCylinderPickerDateComponent,
				CommonsCircularPickerComponent,
				CommonsDialogTimeComponent,
				CommonsDateTimeComponent
		]
})
export class NgxMaterialCommonsEsMobileFormModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsMobileFormModule> {
		return {
				ngModule: NgxMaterialCommonsEsMobileFormModule,
				providers: [
						CommonsSelectorService,
						CommonsDialogTimeService
				]
		};
	}
}
