import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsDrawerService } from 'ngx-materialcommons-es-mobile';

export interface ICommonsSelectorSettings {
		allowNone?: boolean;
}

export type TShowSelector = {
		value: string|undefined;
		options: string[];
		settings: ICommonsSelectorSettings;
		select: (_: string|undefined) => void;
		cancel: () => void;
};

@Injectable()
export class CommonsSelectorService {
	private showEmitter: EventEmitter<TShowSelector> = new EventEmitter<TShowSelector>(true);
	private hideEmitter: EventEmitter<void> = new EventEmitter<void>(true);

	constructor(
			private drawerService: CommonsDrawerService
	) { }

	public select(
			value: string|undefined,
			options: string[],
			settings: ICommonsSelectorSettings,
			select: (_: string|undefined) => void,
			cancel?: () => void
	) {
		this.showEmitter.emit({
				value: value,
				options: options,
				settings: settings,
				select: (v: string|undefined): void => {
					this.hide();
					select(v);
				},
				cancel: (): void => {
					this.hide();
					if (cancel) cancel();
				}
		});
		
		this.drawerService.show('_selector');
	}

	public hide() {
		this.drawerService.hide('_selector');
		
		setTimeout((): void => {
			this.hideEmitter.emit();
		}, 250);	// wait for the 200ms animation to finish
	}
	
	public showObservable(): Observable<TShowSelector> {
		return this.showEmitter;
	}
	
	public hideObservable(): Observable<void> {
		return this.hideEmitter;
	}

}
