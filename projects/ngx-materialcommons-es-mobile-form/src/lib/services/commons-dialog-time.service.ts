import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { ICommonsDialogTime, EDialogTimeType, TCallback } from '../interfaces/icommons-dialog-time';

@Injectable()
export class CommonsDialogTimeService {
	public static generate(
			title: string,
			type: EDialogTimeType,
			timestamp: Date,
			callback: TCallback,
			allowDelete?: boolean
	): ICommonsDialogTime {
		return {
				title: title,
				timestamp: timestamp,
				type: type,
				allowDelete: allowDelete,
				callback: callback
		};
	}

	private subject: Subject<ICommonsDialogTime> = new Subject<ICommonsDialogTime>();

	public observable(): Observable<ICommonsDialogTime> {
		return this.subject.asObservable();
	}
	
	public edit(
			timestamp: Date,
			title: string,
			type: EDialogTimeType,
			callback: (timestamp: Date|undefined) => void,
			allowDelete?: boolean,
			cancelled?: () => void
	): void {
		this.subject.next(CommonsDialogTimeService.generate(
				title,
				type,
				new Date(timestamp.getTime()),
				(save: boolean, t: Date|undefined): void => {
					if (save) callback(t);
					else if (cancelled) cancelled();
				},
				allowDelete
		));
	}

}
