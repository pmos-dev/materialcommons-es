import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ECommonsDrawerDirection } from 'ngx-materialcommons-es-mobile';

import { CommonsSelectorService } from '../../services/commons-selector.service';
import { TShowSelector } from '../../services/commons-selector.service';

@Component({
		selector: 'commons-drawer-selector',
		templateUrl: './commons-drawer-selector.component.html',
		styleUrls: ['./commons-drawer-selector.component.less']
})
export class CommonsDrawerSelectorComponent extends CommonsComponent implements OnInit {
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	show: boolean = false;
	
	options: string[] = [];
	value: string|undefined;
	
	allowNone: boolean = false;

	// these will be set by the subscription data, so are just defined as temporary empty here.
	select: (_: string|undefined) => void = (_: string|undefined): void => {
		// do nothing
	};
	cancel: () => void = (): void => {
		// do nothing
	};
	
	constructor(
			private selectorService: CommonsSelectorService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.selectorService.showObservable(),
				(data: TShowSelector): void => {
					this.value = data.value;
					this.options = data.options;
					this.allowNone = data.settings.allowNone || false;
					
					this.select = data.select;
					this.cancel = data.cancel;
					
					this.show = true;
				}
		);
		
		this.subscribe(
				this.selectorService.hideObservable(),
				(): void => {
					this.show = false;
				}
		);
	}
	
	doCancel(): void {
		this.cancel();
	}
	
	doSelect(value: string|undefined): void {
		this.select(value);
	}

}
