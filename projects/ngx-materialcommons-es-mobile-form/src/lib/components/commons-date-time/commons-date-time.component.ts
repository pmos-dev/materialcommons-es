import { Component, Input, Output, EventEmitter } from '@angular/core';

import { commonsDateDateToYmdHis, commonsDateGetTextMonth } from 'tscommons-es-core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsDialogTimeService } from '../../services/commons-dialog-time.service';

import { EDialogTimeType } from '../../interfaces/icommons-dialog-time';

@Component({
		selector: 'commons-date-time',
		templateUrl: './commons-date-time.component.html',
		styleUrls: ['./commons-date-time.component.less']
})
export class CommonsDateTimeComponent extends CommonsComponent {
	@Input() date: boolean = true;
	@Input() time: boolean = true;
	@Input() disabled: boolean = false;
	@Input() placeholder?: string;
	@Input() helper?: string;
	@Input() error?: string;
	@Input() editable: boolean = true;
	@Input() clearable: boolean = true;

	@Input() timestamp: Date|undefined;
	@Output() timestampChange: EventEmitter<Date|undefined> = new EventEmitter<Date|undefined>();

	focused: boolean = false;
	isDropped: boolean = false;
	
	dropdownName: string;
	
	constructor(
			private dialogTimeService: CommonsDialogTimeService
	) {
		super();
		
		this.dropdownName = `_dropdown_${Math.random()}`;
	}

	getDay(): string {
		if (!this.timestamp) return '';
		
		const formatted: string = commonsDateDateToYmdHis(this.timestamp);
		return formatted.split(' ')[0].split('-')[2];
	}
	
	getMonth(): string {
		if (!this.timestamp) return '';
		
		return commonsDateGetTextMonth(this.timestamp.getMonth()).substring(0, 3);
	}
	
	getYear(): string {
		if (!this.timestamp) return '';
		
		const formatted: string = commonsDateDateToYmdHis(this.timestamp);
		return formatted.split(' ')[0].split('-')[0];
	}
	
	getHour(): string {
		if (!this.timestamp) return '';
		
		const formatted: string = commonsDateDateToYmdHis(this.timestamp);
		return formatted.split(' ')[1].split(':')[0];
	}
	
	getMinute(): string {
		if (!this.timestamp) return '';
		
		const formatted: string = commonsDateDateToYmdHis(this.timestamp);
		return formatted.split(' ')[1].split(':')[1];
	}

	doClick(): void {
		if (this.disabled) return;
		
		let dialogType: EDialogTimeType = EDialogTimeType.DATE;
		if (this.time) dialogType = EDialogTimeType.TIME;
		if (dialogType === EDialogTimeType.TIME && this.date) dialogType = EDialogTimeType.DATETIME;
		
		this.focused = true;
		this.dialogTimeService.edit(
				this.timestamp || new Date(),	// today if undefined
				this.placeholder || 'Edit date time',
				dialogType,
				(timestamp: Date|undefined): void => {
					this.focused = false;
					this.timestampChange.emit(timestamp);
				},
				false,
				(): void => {
					this.focused = false;
				}
		);
	}
	
	doClear(): void {
		if (this.disabled || !this.clearable) return;
		this.timestampChange.emit(undefined);
	}
}
