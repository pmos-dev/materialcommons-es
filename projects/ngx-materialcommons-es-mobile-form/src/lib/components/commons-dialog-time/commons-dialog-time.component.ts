import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsDialogService } from 'ngx-materialcommons-es-app';

import { CommonsDialogTimeService } from '../../services/commons-dialog-time.service';

import { ICommonsDialogTime, EDialogTimeType } from '../../interfaces/icommons-dialog-time';

@Component({
		selector: 'commons-dialog-time',
		templateUrl: './commons-dialog-time.component.html',
		styleUrls: ['./commons-dialog-time.component.less']
})
export class CommonsDialogTimeComponent extends CommonsComponent implements OnInit {
	dialogs: ICommonsDialogTime[] = [];
	stage: EDialogTimeType|undefined = undefined;

	constructor(
			private dialogTimeService: CommonsDialogTimeService,
			private dialogService: CommonsDialogService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.dialogTimeService.observable(),
				(dialog) => {
					this.stage = undefined;
					if (dialog.type === EDialogTimeType.DATETIME) this.stage = EDialogTimeType.DATE;
					
					this.dialogs.push(dialog);
				}
		);
	}
	
	listButtons(dialog: ICommonsDialogTime): string[] {
		const buttons: string[] = [ 'Cancel', this.stage === EDialogTimeType.DATE ? 'Time' : 'Save' ];
		if (dialog.allowDelete) buttons.push('_delete');
		
		return buttons;
	}

	doButton(dialog: ICommonsDialogTime, button: string): void {
		if (button === 'Time') {
			this.stage = EDialogTimeType.TIME;
			return;
		}
		
		this.dialogs.splice(this.dialogs.indexOf(dialog), 1);
		
		if (button === '_delete') {
			this.dialogService.confirm(
					'Delete time',
					`Are you sure you want to delete this ${dialog.type === 'date' ? 'date' : 'time'}?`,
					(): void => {
						dialog.callback(true, undefined);
					}, (): void => {
						// do nothing
					}
			);
		} else {
			dialog.callback(button === 'Save', dialog.timestamp);
		}
	}

}
