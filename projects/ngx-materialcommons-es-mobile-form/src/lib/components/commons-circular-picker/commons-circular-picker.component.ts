import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-circular-picker',
		templateUrl: './commons-circular-picker.component.html',
		styleUrls: ['./commons-circular-picker.component.less']
})
export class CommonsCircularPickerComponent extends CommonsComponent {
	@ViewChild('body', { static: true }) body!: ElementRef;

	@Input() items!: string[];

	@Input() disabled: boolean = false;

	@Input() item?: string;
	@Output() itemChange: EventEmitter<string> = new EventEmitter<string>();

	active: boolean = false;
	angle: number = 0;

	ox?: number;
	oy?: number;
	
	getTrigX(i: number, inset: number): number {
		const size: number = this.body.nativeElement.offsetWidth;
		
		const cx: number = size / 2;
		const px = Math.sin(2 * Math.PI * (i / this.items.length));
		
		return (cx + (inset * px * cx)) - 25;
	}
	getTrigY(i: number, inset: number): number {
		const size: number = this.body.nativeElement.offsetWidth;
		
		const cy: number = size / 2;
		let py = Math.cos(2 * Math.PI * (i / this.items.length));
		py = -py;
		
		return (cy + (inset * py * cy)) - 7;
	}
	
	getRotate(angle: number): string {
		let snap: number = Math.round(angle * this.items.length) / this.items.length;
		if (snap === 1) snap = 0;
		
		const deg: number = 360 * (snap + 0.25);
		return `rotate(${deg}deg)`;
	}
	
	private getAngle(x: number, y: number): number {
		x *= 2;
		x -= 1;
		
		y *= 2;
		y -= 1;
		
		let rad: number = Math.atan2(y, x);
		rad += Math.PI / 2;
		if (rad < 0) rad += Math.PI * 2;
		
		return rad;
	}
	
	getSelectedIndex(): number {
		let snap: number = Math.round(this.angle * this.items.length);
		if (snap === this.items.length) snap = 0;
		
		return snap;
	}
	
	private getSigmaOffsetTop(element: any): number {
		if (element === null) return 0;
		return element.offsetTop + this.getSigmaOffsetTop(element.offsetParent);
	}
	private getSigmaOffsetLeft(element: any): number {
		if (element === null) return 0;
		return element.offsetLeft + this.getSigmaOffsetLeft(element.offsetParent);
	}
	
	doPanStart(event: any): void {
		if (this.disabled) return;
		
		this.active = true;
		
		this.ox = this.getSigmaOffsetLeft(this.body.nativeElement);
		this.oy = this.getSigmaOffsetTop(this.body.nativeElement);
		
		this.handlePan(event);
	}
	
	private handlePan(event: any): void {
		if (this.ox === undefined || this.oy === undefined) return;
	
		let x: number = event.center.x - this.ox;
		let y: number = event.center.y - this.oy;
		
		const inset: number = 10 + (36 / 2);
		
		const size: number = 230 - (inset * 2);
		
		x -= inset;
		y -= inset;
		
		x /= size;
		y /= size;
		
		x = Math.min(1, Math.max(x, 0));
		y = Math.min(1, Math.max(y, 0));
		
		this.angle = this.getAngle(x, y) / (Math.PI * 2);
	}
	
	doPanMove(event: any): void {
		if (this.disabled) return;
		
		this.handlePan(event);
	}

	doPanEnd(event: any): void {
		if (this.disabled) return;
		
		this.handlePan(event);

		this.active = false;
	}
}
