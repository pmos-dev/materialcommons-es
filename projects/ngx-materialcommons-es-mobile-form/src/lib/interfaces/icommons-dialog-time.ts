export enum EDialogTimeType {
	DATE = 'date', TIME = 'time', DATETIME = 'datetime'
}

export type TCallback = (save: boolean, timestamp: Date|undefined) => void;

export interface ICommonsDialogTime {
	title: string;
	timestamp: Date;
	type: EDialogTimeType;
	wrapping?: boolean;
	allowDelete?: boolean;
	
	callback: TCallback;
}
