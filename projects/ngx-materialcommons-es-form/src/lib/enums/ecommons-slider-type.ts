import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsSliderType {
		NUMERIC = 'numeric',
		VALUES = 'values',
		PERCENTAGE = 'percentage'
}

export function toECommonsSliderType(type: string): ECommonsSliderType|undefined {
	switch (type) {
	case ECommonsSliderType.NUMERIC.toString():
		return ECommonsSliderType.NUMERIC;
	case ECommonsSliderType.VALUES.toString():
		return ECommonsSliderType.VALUES;
	case ECommonsSliderType.PERCENTAGE.toString():
		return ECommonsSliderType.PERCENTAGE;
	}
	return undefined;
}

export function fromECommonsSliderType(type: ECommonsSliderType): string {
	switch (type) {
	case ECommonsSliderType.NUMERIC:
		return ECommonsSliderType.NUMERIC.toString();
	case ECommonsSliderType.VALUES:
		return ECommonsSliderType.VALUES.toString();
	case ECommonsSliderType.PERCENTAGE:
		return ECommonsSliderType.PERCENTAGE.toString();
	}
	
	throw new Error('Unknown ECommonsSliderType');
}

export function isECommonsSliderType(test: unknown): test is ECommonsSliderType {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsSliderType(test) !== undefined;
}

export function keyToECommonsSliderType(key: string): ECommonsSliderType {
	switch (key) {
	case 'NUMERIC':
		return ECommonsSliderType.NUMERIC;
	case 'VALUES':
		return ECommonsSliderType.VALUES;
	case 'PERCENTAGE':
		return ECommonsSliderType.PERCENTAGE;
	}
	
	throw new Error(`Unable to obtain ECommonsSliderType for key: ${key}`);
}

export const ECOMMONS_SLIDER_TYPES: ECommonsSliderType[] = Object.keys(ECommonsSliderType)
	.map((e: string): ECommonsSliderType => keyToECommonsSliderType(e));
