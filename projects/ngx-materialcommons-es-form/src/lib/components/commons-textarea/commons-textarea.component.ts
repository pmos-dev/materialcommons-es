import { Component, Input, Output, EventEmitter, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';
import { CommonsFocusService } from 'ngx-angularcommons-es-browser';

@Component({
		selector: 'commons-textarea',
		templateUrl: './commons-textarea.component.html',
		styleUrls: ['./commons-textarea.component.less']
})
export class CommonsTextareaComponent extends CommonsComponent implements OnInit, AfterViewInit {
	@ViewChild('textarea', { static: true }) private textarea: ElementRef|undefined;

	@Input() disabled: boolean = false;
	@Input() placeholder?: string;
	@Input() helper?: string;
	@Input() error?: string;
	@Input() focusId?: string;
	@Input() maxLength?: number;

	@Output() valueChange: EventEmitter<string|undefined> = new EventEmitter<string|undefined>();
	@Input() value: string|undefined|null;

	@Output() lostFocusAndChanged: EventEmitter<void> = new EventEmitter<void>(true);
	
	focused: boolean = false;

	lastFocusValue?: string|undefined|null = undefined;
	
	constructor(
			private focusService: CommonsFocusService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.lastFocusValue = this.value;
	}

	ngAfterViewInit(): void {
		this.subscribe(
				this.focusService.elementFocusObservable(),
				(id: string): void => {
					if (!this.focusId) return;
					if (this.focusId !== id) return;
					if (!this.textarea) return;
					
					this.textarea.nativeElement.focus();
				}
		);
	}

	getValue(): string {
		if (this.value === undefined) return '';
		if (this.value === null) return '';
		return this.value;
	}
	
	doChanged(event: string|undefined): void {
		if (this.disabled) return;
		
		this.valueChange.emit(event);
	}
	
	doFocus(state: boolean): void {
		this.focused = state;

		if (this.focused) this.lastFocusValue = this.value;
		else {
			if (this.lastFocusValue !== this.value) this.lostFocusAndChanged.emit();
		}
	}
}
