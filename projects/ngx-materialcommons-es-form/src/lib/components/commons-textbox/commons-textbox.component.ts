import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild, HostBinding, ElementRef } from '@angular/core';

import { CommonsFinalValue } from 'tscommons-es-async';
import { TCommonsFinalValue } from 'tscommons-es-async';

import { CommonsComponent } from 'ngx-angularcommons-es-core';
import { CommonsFocusService } from 'ngx-angularcommons-es-browser';

import { CommonsMenuService } from 'ngx-materialcommons-es-app';

@Component({
		selector: 'commons-textbox',
		templateUrl: './commons-textbox.component.html',
		styleUrls: ['./commons-textbox.component.less']
})
export class CommonsTextboxComponent extends CommonsComponent implements OnInit, AfterViewInit {
	@HostBinding('class.forefront') forefront!: boolean;

	@ViewChild('textbox', { static: true }) private textbox: ElementRef|undefined;
	
	@Input() type: string = 'text';
	@Input() disabled: boolean = false;
	@Input() placeholder?: string;
	@Input() helper?: string;
	@Input() error?: string;
	@Input() options: string[] = [];
	@Input() optionsOnly: boolean = false;
	@Input() editable: boolean = true;
	@Input() clearable: boolean = true;
	@Input() customIcon?: string;
	@Input() autocompleteDelay: number = 500;
	@Input() autocompleteLength: number = 3;
	@Input() focusId?: string;

	@Input() value: string|undefined|null;
	@Output() valueChange: EventEmitter<string|undefined> = new EventEmitter<string|undefined>();

	@Output() edit: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() lostFocusAndChanged: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() enterPressed: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() escPressed: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() selectChanged: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() custom: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() autocomplete: EventEmitter<string> = new EventEmitter<string>(true);
	@Output() cleared: EventEmitter<void> = new EventEmitter<void>(true);
	
	focused: boolean = false;
	isDropped: boolean = false;
	
	dropdownName: string;
	
	lastFocusValue: string|undefined|null;

	private autocompleteFinalValue: CommonsFinalValue<string>|undefined;
	
	constructor(
			private menuService: CommonsMenuService,
			private focusService: CommonsFocusService
	) {
		super();
		
		this.dropdownName = `_dropdown_${Math.random()}`;
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.menuService.showObservable(),
				(name: string): void => {
					if (name === this.dropdownName) {
						this.forefront = true;
						this.isDropped = true;
					}
				}
		);
		this.subscribe(
				this.menuService.hideObservable(),
				(name: string): void => {
					if (name === this.dropdownName) {
						this.forefront = false;
						this.isDropped = false;
					}
				}
		);
		
		this.lastFocusValue = this.value;
		
		this.autocompleteFinalValue = new CommonsFinalValue<string>(this.autocompleteDelay);
		
		this.subscribe(
				this.autocompleteFinalValue.releaseObservable(),
				(data: TCommonsFinalValue<string>): void => {
					this.autocomplete.emit(data.value);
				}
		);
	}

	ngAfterViewInit(): void {
		this.subscribe(
				this.focusService.elementFocusObservable(),
				(id: string): void => {
					if (!this.focusId) return;
					if (this.focusId !== id) return;
					if (!this.textbox) return;
					
					this.textbox.nativeElement.focus();
				}
		);
	}

	getType(): string {
		if (this.type === undefined) return 'text';
		return this.type;
	}
	
	getValue(): string {
		if (this.value === undefined) return '';
		if (this.value === null) return '';
		return this.value;
	}
	
	doChanged(event: string|undefined): void {
		if (this.disabled) return;
		
		this.valueChange.emit(event);

		if (
			!this.value
				|| !this.autocompleteFinalValue
				|| 'string' !== typeof this.value
				|| this.value.length < this.autocompleteLength
		) return;
		
		this.autocompleteFinalValue.push(this.value);
	}
	
	doFocus(state: boolean): void {
		this.focused = state;

		if (this.focused) this.lastFocusValue = this.value;
		else {
			if (this.lastFocusValue !== this.value) this.lostFocusAndChanged.emit();
		}
	}
	
	doDropdown(): void {
		if (this.disabled) return;
		
		if (this.textbox) this.textbox.nativeElement.focus();
		this.menuService.show(this.dropdownName);
	}
	
	doSelect(option: string): void {
		if (this.disabled) return;
		
		this.valueChange.emit(option);
		if (this.textbox) this.textbox.nativeElement.focus();
		
		this.selectChanged.emit();
	}
	
	doClick(): void {
		if (this.disabled) return;
		
		if (this.optionsOnly) this.doDropdown();
		if (this.editable) this.edit.emit();
	}
	
	doEnterPressed(): void {
		if (this.disabled) return;
		
		this.enterPressed.emit();
	}
	doEscPressed(): void {
		if (this.disabled) return;
		
		this.value = this.lastFocusValue;
		this.valueChange.emit(this.value === null ? undefined : this.value);
		this.escPressed.emit();
	}
	
	doClear(): void {
		if (this.disabled || !this.clearable) return;
		this.valueChange.emit('');
		this.cleared.emit();
	}
	
	doCustomIcon(): void {
		if (this.disabled || !this.customIcon) return;
		this.custom.emit();
	}
}
