import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';
import { NgxAngularCommonsEsBrowserModule } from 'ngx-angularcommons-es-browser';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';

import { CommonsCheckboxComponent } from './components/commons-checkbox/commons-checkbox.component';
import { CommonsTextboxComponent } from './components/commons-textbox/commons-textbox.component';
import { CommonsSliderComponent } from './components/commons-slider/commons-slider.component';
import { CommonsTextareaComponent } from './components/commons-textarea/commons-textarea.component';
import { CommonsRadioComponent } from './components/commons-radio/commons-radio.component';

@NgModule({
		imports: [
				CommonModule,
				FormsModule,
				NgxAngularCommonsEsCoreModule,
				NgxAngularCommonsEsPipeModule,
				NgxAngularCommonsEsBrowserModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsAppModule
		],
		declarations: [
				CommonsTextboxComponent,
				CommonsCheckboxComponent,
				CommonsSliderComponent,
				CommonsTextareaComponent,
				CommonsRadioComponent
		],
		exports: [
				CommonsTextboxComponent,
				CommonsCheckboxComponent,
				CommonsSliderComponent,
				CommonsTextareaComponent,
				CommonsRadioComponent
		]
})
export class NgxMaterialCommonsEsFormModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsFormModule> {
		return {
				ngModule: NgxMaterialCommonsEsFormModule,
				providers: [
				]
		};
	}
}
