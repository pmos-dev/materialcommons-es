import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsDrawerService } from 'ngx-materialcommons-es-mobile';

import { ICommonsKeyboard } from '../interfaces/icommons-keyboard';

export interface ICommonsEditorSettings {
		keyboard: ICommonsKeyboard;
	
		allowEmpty?: boolean;
		maxLength?: number;
		minLength?: number;
}

export type TShowEditor = {
		value: string;
		settings: ICommonsEditorSettings;
		ok: (_: string) => void;
		cancel: () => void;
};

@Injectable()
export class CommonsEditorService {
	private showEmitter: EventEmitter<TShowEditor> = new EventEmitter<TShowEditor>(true);
	
	private hideEmitter: EventEmitter<void> = new EventEmitter<void>(true);

	constructor(
			private drawerService: CommonsDrawerService
	) { }

	public edit(
			value: string,
			settings: ICommonsEditorSettings,
			ok: (_: string) => void,
			cancel?: () => void
	) {
		this.showEmitter.emit({
				value: value,
				settings: settings,
				ok: (v: string): void => {
					this.hide();
					ok(v);
				},
				cancel: (): void => {
					this.hide();
					if (cancel) cancel();
				}
		});
		
		this.drawerService.show('_editor');
	}

	public hide() {
		this.drawerService.hide('_editor');
		
		setTimeout((): void => { this.hideEmitter.emit(); }, 250);	// wait for the 200ms animation to finish
	}
	
	public showObservable(): Observable<TShowEditor> {
		return this.showEmitter;
	}
	
	public hideObservable(): Observable<void> {
		return this.hideEmitter;
	}

}
