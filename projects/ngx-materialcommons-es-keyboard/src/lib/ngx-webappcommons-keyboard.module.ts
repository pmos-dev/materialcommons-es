import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsMobileModule } from 'ngx-materialcommons-es-mobile';

import { CommonsKeyboardKeyComponent } from './components/commons-keyboard-key/commons-keyboard-key.component';
import { CommonsKeyboardComponent } from './components/commons-keyboard/commons-keyboard.component';
import { CommonsEditorComponent } from './components/commons-editor/commons-editor.component';
import { CommonsEditorDisplayComponent } from './components/commons-editor-display/commons-editor-display.component';
import { CommonsDrawerEditorComponent } from './components/commons-drawer-editor/commons-drawer-editor.component';

import { CommonsEditorService } from './services/commons-editor.service';

@NgModule({
		imports: [
				CommonModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsMobileModule
		],
		declarations: [
				CommonsKeyboardKeyComponent,
				CommonsKeyboardComponent,
				CommonsEditorComponent,
				CommonsEditorDisplayComponent,
				CommonsDrawerEditorComponent
		],
		exports: [
				CommonsKeyboardComponent,
				CommonsEditorComponent,
				CommonsDrawerEditorComponent
		]
})
export class NgxMaterialCommonsEsKeyboardModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsKeyboardModule> {
		return {
				ngModule: NgxMaterialCommonsEsKeyboardModule,
				providers: [
					CommonsEditorService
				]
		};
	}
}
