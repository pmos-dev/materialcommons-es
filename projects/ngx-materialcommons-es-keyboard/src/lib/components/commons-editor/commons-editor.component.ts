import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ICommonsKeyboard } from '../../interfaces/icommons-keyboard';

@Component({
		selector: 'commons-editor',
		templateUrl: './commons-editor.component.html',
		styleUrls: ['./commons-editor.component.less']
})
export class CommonsEditorComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() value: string = '';
	@Input() allowEmpty: boolean = false;
	@Input() maxLength: number = 255;
	@Input() minLength: number = 0;
	
	@Input() keyboard!: ICommonsKeyboard;
	
	@Output() cancel: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onOk: EventEmitter<string> = new EventEmitter<string>(true);

	characters: string[] = [];
	private position: number = 0;
	pre: string = '';
	post: string = '';
	
	ngOnInit(): void {
		super.ngOnInit();

		this.characters = this.value.split('');
		this.position = this.characters.length;
		
		this.rebuild();
	}
	
	ngOnChanges(): void {
		this.characters = this.value.split('');
		this.position = this.characters.length;
		
		this.rebuild();
	}

	private rebuild(): void {
		this.pre = this.characters.slice(0, this.position).join('');
		this.post = this.characters.slice(this.position).join('');
	}
	
	private append(character: string): void {
		this.characters.splice(this.position, 0, character);
		this.position++;
		
		this.rebuild();
	}
	
	doCharacter(character: string): void {
		this.append(character);
	}

	doOk(): void {
		this.onOk.emit(this.pre + this.post);
	}

	doCancel(): void {
		this.onCancel.emit();
	}

	doBackspace(): void {
		if (this.position === 0) return;
		
		this.position--;
		this.characters.splice(this.position, 1);
		
		this.rebuild();
	}
	
	doMove(index: number): void {
		this.position = index === -1 ? this.characters.length : index;
		
		this.rebuild();
	}

}
