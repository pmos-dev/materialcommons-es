import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ICommonsKeyboardKey } from '../../interfaces/icommons-keyboard-key';

@Component({
		selector: 'commons-keyboard-key',
		templateUrl: './commons-keyboard-key.component.html',
		styleUrls: ['./commons-keyboard-key.component.less']
})
export class CommonsKeyboardKeyComponent extends CommonsComponent {
	@Input() key!: ICommonsKeyboardKey;
	@Input() setLabels: {} = {};
	@Input() disabled: boolean = false;
	@Input() disableThemeInvert: boolean = false;

	@Input() maxColumns!: number;

	@Output() onCharacter: EventEmitter<string> = new EventEmitter<string>(true);
	@Output() onSpace: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onOk: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() cancel: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onBackspace: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onSet: EventEmitter<string> = new EventEmitter<string>(true);

	doClick(): void {
		if (this.key.cancel) {
			this.onCancel.emit();
			return;
		}
		if (this.key.space) {
			this.onSpace.emit();
			return;
		}
		if (this.key.ok) {
			this.onOk.emit();
			return;
		}
		if (this.key.backspace) {
			this.onBackspace.emit();
			return;
		}
		if (this.key.set !== undefined) {
			this.onSet.emit(this.key.set);
			return;
		}
		if (this.key.character !== undefined) {
			this.onCharacter.emit(this.key.character);
			return;
		}
	}

}
