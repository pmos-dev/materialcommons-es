import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';

import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';

import { CommonsAvatarFeedComponent } from './components/commons-avatar-feed/commons-avatar-feed.component';
import { CommonsAvatarPostComponent } from './components/commons-avatar-post/commons-avatar-post.component';
import { CommonsAvatarPostTimestampComponent } from './components/commons-avatar-post-timestamp/commons-avatar-post-timestamp.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsPipeModule,
				NgxMaterialCommonsEsAppModule
		],
		declarations: [
				CommonsAvatarFeedComponent,
				CommonsAvatarPostComponent,
				CommonsAvatarPostTimestampComponent
		],
		exports: [
				CommonsAvatarFeedComponent
		]
})
export class NgxMaterialCommonsEsFeedModule { }
