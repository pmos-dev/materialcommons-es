import { Component, Input, OnInit } from '@angular/core';

import { CommonsManualChangeDetectionComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-avatar-post-timestamp',
		templateUrl: './commons-avatar-post-timestamp.component.html',
		styleUrls: ['./commons-avatar-post-timestamp.component.less']
})
export class CommonsAvatarPostTimestampComponent extends CommonsManualChangeDetectionComponent implements OnInit {
	@Input() timestamp: Date|undefined;
	@Input() absolute: boolean = false;
	
	now: Date = new Date();

	ngOnInit(): void {
		super.ngOnInit();

		this.setDetachedInterval(
				0,
				1000,
				(): void => {
					this.now = new Date();
					
					this.runChangeDetection();
				}
		);
	}
}
