import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsFormModule } from 'ngx-materialcommons-es-form';

import { CommonsCameraComponent } from './components/commons-camera/commons-camera.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsCoreModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsFormModule
		],
		declarations: [
				CommonsCameraComponent
		],
		exports: [
				CommonsCameraComponent
		]
})
export class NgxMaterialCommonsEsCameraModule {}
