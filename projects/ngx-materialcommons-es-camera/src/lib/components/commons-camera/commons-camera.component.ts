import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';

import { commonsAsyncTimeout } from 'tscommons-es-async';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ECommonsCameraSnapshotImageFormat } from '../../enums/ecommons-snapshot-image-format';

@Component({
		selector: 'commons-camera',
		templateUrl: './commons-camera.component.html',
		styleUrls: ['./commons-camera.component.less']
})
export class CommonsCameraComponent extends CommonsComponent implements AfterViewInit {
	@ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>|undefined;
	@ViewChildren('player') player: QueryList<ElementRef<HTMLVideoElement>>|undefined;

	@Input() enabled: boolean = false;
	@Input() snapshotImageFormat: ECommonsCameraSnapshotImageFormat = ECommonsCameraSnapshotImageFormat.JPEG;
	@Input() autoSelectFirstIfSingular: boolean = false;
	@Input() clearable: boolean = false;

	@Output() selected: EventEmitter<MediaDeviceInfo|undefined> = new EventEmitter<MediaDeviceInfo|undefined>(true);
	@Output() snapshot: EventEmitter<string> = new EventEmitter<string>(true);
	@Output() reattempt: EventEmitter<void> = new EventEmitter<void>(true);

	videos: MediaDeviceInfo[] = [];
	videoNames: string[] = [];
	videoName: string|undefined;

	currentPlayer: HTMLVideoElement|undefined;

	width: number = 1;
	height: number = 1;
	isVideoVisible: boolean = false;
	isLive: boolean = true;

	// eslint-disable-next-line @typescript-eslint/no-useless-constructor
	constructor() {
		super();
	}

	override async ngAfterViewInit(): Promise<void> {
		super.ngAfterViewInit();

		const devices: MediaDeviceInfo[] = await navigator.mediaDevices.enumerateDevices();
		this.videos = devices
				.filter((device: MediaDeviceInfo): boolean => device.kind === 'videoinput');
		this.videoNames = this.videos
				.map((video: MediaDeviceInfo): string => video.label);

		if (this.autoSelectFirstIfSingular && this.videoNames.length === 1) {
			this.videoName = this.videoNames[0];
			this.doSetVideo();
		}
	}

	async doSetVideo(): Promise<void> {
		if (!this.player) return;

		this.currentPlayer = undefined;

		this.isVideoVisible = false;
		await commonsAsyncTimeout(10);

		const match: MediaDeviceInfo|undefined = this.videos
				.find((video: MediaDeviceInfo): boolean => video.label === this.videoName);
		if (!match) return;

		this.isVideoVisible = true;
		await commonsAsyncTimeout(10);

		if (this.player.length < 1) throw new Error('No video element has been created');

		const player: HTMLVideoElement = this.player.get(0)!.nativeElement;

		const constraints: MediaStreamConstraints = {
				video: { deviceId: { exact: match.deviceId } }
		};

		try {
			const stream: MediaStream = await navigator.mediaDevices.getUserMedia(constraints);
			player.srcObject = stream;
		} catch (e) {
			const stream: MediaStream = await navigator.mediaDevices.getUserMedia({ video: true });
			player.srcObject = stream;
		}

		player.load();

		await commonsAsyncTimeout(100);
		await player.play();

		this.currentPlayer = player;
		this.selected.emit(match);
	}

	async takeSnapshot(): Promise<void> {
		if (!this.enabled) return;
		if (!this.currentPlayer || !this.canvas) return;
		
		const player: HTMLVideoElement = this.currentPlayer;
		const canvas: HTMLCanvasElement = this.canvas.nativeElement;

		this.width = player.clientWidth;
		this.height = player.clientHeight;

		await commonsAsyncTimeout(10);

		const context: CanvasRenderingContext2D|null = canvas.getContext('2d');
		if (!context) return;

		context.drawImage(
				player,
				0,
				0,
				player.videoWidth,
				player.videoHeight,
				0,
				0,
				this.width,
				this.height
		);

		const jpeg: string = canvas.toDataURL(`image/${this.snapshotImageFormat}`);
		if (!/^data:image\/[a-z]{3,4};base64,/.test(jpeg)) throw new Error('Invalid image data or not base64 encoded.');

		this.isLive = false;

		this.snapshot.emit(jpeg);
	}

	resetToLive(): void {
		this.isLive = true;
		this.reattempt.emit();
	}
}
