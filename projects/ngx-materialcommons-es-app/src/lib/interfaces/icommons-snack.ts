import { ECommonsSnackType } from '../enums/ecommons-snack-type';

export interface ICommonsSnack {
		type: ECommonsSnackType;
		message: string;
		timestamp: number;
}
