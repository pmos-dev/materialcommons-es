import { EMaterialDesignIcons } from 'ngx-materialcommons-es-core';

export interface ICommonsAvatar {
		icon?: string;
		iconSet?: EMaterialDesignIcons;
		letter?: string;
		image?: string;
		waitRotate?: boolean;
		
		background?: string;
		foreground?: string;
}
