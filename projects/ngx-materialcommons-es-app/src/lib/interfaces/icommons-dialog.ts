export interface ICommonsDialog {
		title: string;
		message?: string;
		buttons: string[];
		countdown?: number;
		defaultButton?: string;
		
		callback: (button: string) => void;
		clickOff?: () => void;
}
