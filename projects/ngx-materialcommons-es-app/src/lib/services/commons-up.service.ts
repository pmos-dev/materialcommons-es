import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class CommonsUpService {
	private titleSubject: EventEmitter<string|null> = new EventEmitter<string|null>(true);
	
	private callback: (() => void)|undefined;
	
	// only the app.component or similar should subscribe to this one.
	public titleObservable(): Observable<string|null> {
		return this.titleSubject;
	}
	
	public down(
			title: string,
			callback: () => void
	): void {
		// only supporting one-level flat at the moment
		if (this.callback !== undefined) return;
		
		this.callback = callback;

		this.titleSubject.emit(title);
	}
	
	public up(): void {
		if (!this.callback) throw new Error('Attempting to move up where no callback is present');
		
		this.callback();
		
		this.callback = undefined;
	}
	
	public reset(): void {
		this.titleSubject.emit(undefined);
		this.callback = undefined;
	}
}
