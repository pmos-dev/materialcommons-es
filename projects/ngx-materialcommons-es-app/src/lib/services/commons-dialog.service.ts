import { Injectable } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { ICommonsDialog } from '../interfaces/icommons-dialog';

@Injectable()
export class CommonsDialogService {
	private static generate(
			title: string,
			message: string,
			buttons: string[],
			callback: (button: string) => void,
			clickOff?: () => void,
			countdown?: number,
			defaultButton?: string
	): ICommonsDialog {
		return {
				title: title,
				message: message,
				buttons: buttons,
				countdown: countdown,
				defaultButton: defaultButton,
				callback: callback,
				clickOff: clickOff
		};
	}
	
	private onDialog: Subject<ICommonsDialog> = new Subject<ICommonsDialog>();

	public observable(): Observable<ICommonsDialog> {
		return this.onDialog.asObservable();
	}
	
	public ok(title: string, message: string, callback: () => void): void {
		this.onDialog.next(CommonsDialogService.generate(
				title,
				message,
				[ 'OK' ],
				(_): void => callback(),
				(): void => callback(),
				undefined,
				'OK'
		));
	}

	public cancel(title: string, message: string, callback: () => void): void {
		this.onDialog.next(CommonsDialogService.generate(
				title,
				message,
				[ 'Cancel' ],
				(_): void => callback(),
				(): void => callback(),
				undefined,
				'Cancel'
		));
	}

	public confirm(title: string, message: string, ok: () => any, cancel: () => void, countdown?: number): void {
		this.onDialog.next(CommonsDialogService.generate(
				title,
				message,
				[ 'Cancel', 'OK' ],
				(button: string): void => {
					if (button === 'OK') ok();
					if (button === 'Cancel') cancel();
				},
				(): void => cancel(),
				countdown,
				'OK'
		));
	}

	public deleteConfirm(
			title: string,
			message: string,
			deleteCallback: () => (void|Promise<void>),	// the word delete is a reserved word
			cancel: () => void,
			countdown?: number
	): void {
		this.onDialog.next(CommonsDialogService.generate(
				title,
				message,
				[ 'Cancel', '_delete' ],
				(button: string): void => {
					if (button === '_delete') deleteCallback();
					if (button === 'Cancel') cancel();
				},
				(): void => cancel(),
				countdown,
				'_delete'
		));
	}

	public yesno(
			title: string,
			message: string,
			yes: () => any,
			no: () => void,
			countdown?: number
	): void {
		this.onDialog.next(CommonsDialogService.generate(
				title,
				message,
				[ 'No', 'Yes' ],
				(button: string): void => {
					if (button === 'Yes') yes();
					if (button === 'No') no();
				},
				undefined,	// don't allow clickOff for Yes/No
				countdown,
				'Yes'
		));
	}
}
