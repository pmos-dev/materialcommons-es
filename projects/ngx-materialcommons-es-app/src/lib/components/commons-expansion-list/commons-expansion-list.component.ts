import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-expansion-list]',
		templateUrl: './commons-expansion-list.component.html',
		styleUrls: ['./commons-expansion-list.component.less']
})
export class CommonsExpansionListComponent extends CommonsComponent {
	@Input() noTopGap: boolean = false;
	@Input() noBottomGap: boolean = false;
	@Input() biggerTopGap: boolean = false;
}
