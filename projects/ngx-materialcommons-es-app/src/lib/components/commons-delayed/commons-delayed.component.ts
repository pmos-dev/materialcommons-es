import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';

import { CommonsComponent } from 'ngx-angularcommons-es-core';
import { CommonsDelayedService } from 'ngx-angularcommons-es-app';

@Component({
		selector: 'commons-delayed',
		templateUrl: './commons-delayed.component.html',
		styleUrls: ['./commons-delayed.component.less'],
		animations: [
				trigger('delayedAnimation', [
						transition('void => *', [
								style({ opacity: 0 }),
								animate('100ms', style({ opacity: 1 }))
						]),
						transition('* => void', [
								style({ opacity: 1 }),
								animate('100ms', style({ opacity: 0 }))
						])
				])
		]
})
export class CommonsDelayedComponent extends CommonsComponent implements OnInit {
	routing: boolean;
	delayed: boolean;
	
	constructor(
		private router: Router,
		private delayedService: CommonsDelayedService
	) {
		super();
		
		this.routing = false;
		this.delayed = false;
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.router.events,
				(event: any): void => {	// there is no common parent class for these
					if (event instanceof NavigationStart) {
						this.routing = true;
					}
					if (
						event instanceof NavigationEnd
					|| event instanceof NavigationCancel
					|| event instanceof NavigationError
					) {
						this.routing = false;
					}
				}
		);
		
		this.subscribe(
				this.delayedService.delayedObservable(),
				(state: boolean): void => {
					this.delayed = state;
				}
		);
	}
}
