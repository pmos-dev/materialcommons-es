import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsDialogService } from '../../services/commons-dialog.service';

import { ICommonsDialog } from '../../interfaces/icommons-dialog';

@Component({
		selector: 'commons-dialogs',
		templateUrl: './commons-dialogs.component.html',
		styleUrls: ['./commons-dialogs.component.less']
})
export class CommonsDialogsComponent extends CommonsComponent implements OnInit {
	dialogs: ICommonsDialog[] = [];
	
	constructor(
		private dialogService: CommonsDialogService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.dialogService.observable(),
				(dialog) => {
					this.dialogs.push(dialog);
				}
		);
	}

	doButton(dialog: ICommonsDialog, button: string): void {
		this.dialogs.splice(this.dialogs.indexOf(dialog), 1);
		
		dialog.callback(button);
	}

	doClickOff(dialog: ICommonsDialog): void {
		if (!dialog.clickOff) return;
		
		this.dialogs.splice(this.dialogs.indexOf(dialog), 1);
		
		dialog.clickOff();
	}
}
