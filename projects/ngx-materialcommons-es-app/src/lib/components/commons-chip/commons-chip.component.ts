import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { EMaterialDesignIcons } from 'ngx-materialcommons-es-core';

import { ICommonsAvatar } from '../../interfaces/icommons-avatar';

const USE_IMG_RATHER_THAN_LABEL_BACKGROUND_BUG: boolean = true;

@Component({
		selector: 'commons-chip',
		templateUrl: './commons-chip.component.html',
		styleUrls: ['./commons-chip.component.less']
})
export class CommonsChipComponent extends CommonsComponent {
	USE_IMG_RATHER_THAN_LABEL_BACKGROUND_BUG = USE_IMG_RATHER_THAN_LABEL_BACKGROUND_BUG;
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	@Input() avatar?: ICommonsAvatar;
	@Input() canCancel: boolean = false;
	@Input() disabled: boolean = false;
	@Input() unselected?: boolean;	// used by CommonsChipsFilter
	
	@Output() cancel: EventEmitter<void> = new EventEmitter<void>();

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}

	doCancel(): void {
		this.cancel.emit();
	}
	
	getIconSet(): EMaterialDesignIcons {
		// just to satisfy the StrictNullCheck
		if (!this.avatar) return EMaterialDesignIcons.MATERIALDESIGN;
		
		return this.avatar.iconSet || EMaterialDesignIcons.MATERIALDESIGN;
	}
	
	getUrl(): SafeStyle|undefined {
		if (!this.avatar) return undefined;
		return this.sanitized.bypassSecurityTrustStyle(`url('${this.avatar.image}')`);
	}
}
