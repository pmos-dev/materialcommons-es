import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

import { timer } from 'rxjs';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ICommonsSnack } from '../../interfaces/icommons-snack';

import { ECommonsSnackType } from '../../enums/ecommons-snack-type';

@Component({
		selector: 'commons-snack',
		templateUrl: './commons-snack.component.html',
		styleUrls: ['./commons-snack.component.less']
})
export class CommonsSnackComponent extends CommonsComponent implements OnInit {
	@Input() timeout!: number;
	@Input() allowIgnore: boolean = true;
	@Input() snack!: ICommonsSnack;
	@Input() index!: number;
	
	@Output() ignore: EventEmitter<void> = new EventEmitter<void>();

	ngOnInit(): void {
		super.ngOnInit();
		
		if (this.timeout) {
			this.subscribe(
					timer(this.timeout),
					() => this.doIgnore()
			);
		}
	}
	
	isError(): boolean {
		return this.snack && this.snack.type === ECommonsSnackType.ERROR;
	}

	isSuccess(): boolean {
		return this.snack && this.snack.type === ECommonsSnackType.SUCCESS;
	}

	isNotification(): boolean {
		return this.snack && this.snack.type === ECommonsSnackType.NOTIFICATION;
	}

	doIgnore(): void {
		this.ignore.emit();
	}
}
