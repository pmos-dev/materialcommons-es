import { Component, Input, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';
import { CommonsLoadingService } from 'ngx-angularcommons-es-app';

@Component({
		selector: 'commons-loading',
		templateUrl: './commons-loading.component.html',
		styleUrls: ['./commons-loading.component.less']
})
export class CommonsLoadingComponent extends CommonsComponent implements OnInit {
	@Input() message: string = 'Loading...';
	
	loading: boolean = true;

	constructor(
			private loadingService: CommonsLoadingService
	) {
		super();
	}

	ngOnInit(): void {
		this.subscribe(
				this.loadingService.loadedObservable(),
				(): void => {
					this.loading = false;
				}
		);
	}

}
