import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-menu-divider',
		templateUrl: './commons-menu-divider.component.html',
		styleUrls: ['./commons-menu-divider.component.less']
})
export class CommonsMenuDividerComponent extends CommonsComponent {
}
