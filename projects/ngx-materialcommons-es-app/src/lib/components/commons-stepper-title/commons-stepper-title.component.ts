import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-stepper-title]',
		templateUrl: './commons-stepper-title.component.html',
		styleUrls: ['./commons-stepper-title.component.less']
})
export class CommonsStepperTitleComponent extends CommonsComponent {
	@Input() step?: number;
	@Input() icon?: string;
}
