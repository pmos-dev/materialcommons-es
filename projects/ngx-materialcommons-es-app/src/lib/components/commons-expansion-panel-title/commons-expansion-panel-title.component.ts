import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-expansion-panel-title]',
		templateUrl: './commons-expansion-panel-title.component.html',
		styleUrls: ['./commons-expansion-panel-title.component.less']
})
export class CommonsExpansionPanelTitleComponent extends CommonsComponent {
	@Input() expanded: boolean = false;

	@Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>(true);

	doToggle(): void {
		this.toggle.emit(!this.expanded);
	}
}
