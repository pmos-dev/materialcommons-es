import { Component, Input, HostListener } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsMenuService } from '../../services/commons-menu.service';

@Component({
		selector: 'commons-inline-menu-item',
		templateUrl: './commons-inline-menu-item.component.html',
		styleUrls: ['./commons-inline-menu-item.component.less']
})
export class CommonsInlineMenuItemComponent extends CommonsComponent {
	@Input() autoClose!: boolean;
	@Input() menu!: string;
	@Input() none!: boolean;

	@HostListener('click') doClose(): void {
		if (this.autoClose) this.menuService.hide(this.menu);
	}

	constructor(
			private menuService: CommonsMenuService
	) {
		super();
	}

	public close(): void {
		this.menuService.hide(this.menu);
	}
}
