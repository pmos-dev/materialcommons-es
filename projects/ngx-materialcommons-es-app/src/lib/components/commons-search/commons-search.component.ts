import { Component, Input, Output, EventEmitter, ViewChild, HostBinding, ElementRef } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: 'commons-search',
		templateUrl: './commons-search.component.html',
		styleUrls: ['./commons-search.component.less']
})
export class CommonsSearchComponent extends CommonsComponent {
	@HostBinding('class.forefront') forefront!: boolean;

	@ViewChild('textbox', { static: true }) textbox!: ElementRef;

	@Input() disabled!: boolean;

	@Output() termChange: EventEmitter<string|undefined> = new EventEmitter<string|undefined>();
	@Input() term?: string;

	focused: boolean = false;
	memory: string|undefined = '';

	doFocus(state: boolean): void {
		this.focused = state;

		if (state) this.memory = this.term;
	}
	
	doChanged(event: string|undefined): void {
		this.termChange.emit(event);
	}

	doReset(): void {
		this.term = undefined;	// odd bug that seems to be needed to reset more than once
		this.termChange.emit(undefined);
	}
	
	doRevert(): void {
		this.term = this.memory;	// odd bug that seems to be needed to reset more than once
		this.termChange.emit(this.memory);
	}
}
