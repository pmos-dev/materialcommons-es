import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsEsCoreModule } from 'ngx-angularcommons-es-core';
import { NgxAngularCommonsEsBrowserModule } from 'ngx-angularcommons-es-browser';
import { NgxAngularCommonsEsPipeModule } from 'ngx-angularcommons-es-pipe';

import { NgxMaterialCommonsEsCoreModule } from 'ngx-materialcommons-es-core';
import { NgxMaterialCommonsEsFormModule } from 'ngx-materialcommons-es-form';

import { CommonsTableComponent } from './components/commons-table/commons-table.component';
import { CommonsTableHeaderComponent } from './components/commons-table-header/commons-table-header.component';
import { CommonsTableHeaderFilterComponent } from './components/commons-table-header-filter/commons-table-header-filter.component';
import { CommonsTableHeaderColumnsComponent } from './components/commons-table-header-columns/commons-table-header-columns.component';
import { CommonsTableHeaderColumnComponent } from './components/commons-table-header-column/commons-table-header-column.component';
import { CommonsTableBodyComponent } from './components/commons-table-body/commons-table-body.component';
import { CommonsTableBodyRowComponent } from './components/commons-table-body-row/commons-table-body-row.component';
import { CommonsTableBodyCellComponent } from './components/commons-table-body-cell/commons-table-body-cell.component';
import { CommonsTableFooterComponent } from './components/commons-table-footer/commons-table-footer.component';
import { CommonsTableFooterPaginationComponent } from './components/commons-table-footer-pagination/commons-table-footer-pagination.component';

import { CommonsTableService } from './services/commons-table.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsEsCoreModule,
				NgxAngularCommonsEsBrowserModule,
				NgxAngularCommonsEsPipeModule,
				NgxMaterialCommonsEsCoreModule,
				NgxMaterialCommonsEsFormModule
		],
		declarations: [
				CommonsTableComponent,
				CommonsTableHeaderComponent,
				CommonsTableHeaderFilterComponent,
				CommonsTableHeaderColumnsComponent,
				CommonsTableHeaderColumnComponent,
				CommonsTableBodyComponent,
				CommonsTableBodyRowComponent,
				CommonsTableBodyCellComponent,
				CommonsTableFooterComponent,
				CommonsTableFooterPaginationComponent
		],
		exports: [
				CommonsTableComponent,
				CommonsTableHeaderComponent,
				CommonsTableHeaderFilterComponent,
				CommonsTableHeaderColumnsComponent,
				CommonsTableHeaderColumnComponent,
				CommonsTableBodyComponent,
				CommonsTableBodyRowComponent,
				CommonsTableBodyCellComponent,
				CommonsTableFooterComponent,
				CommonsTableFooterPaginationComponent
		]
})
export class NgxMaterialCommonsEsTableModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsTableModule> {
		return {
				ngModule: NgxMaterialCommonsEsTableModule,
				providers: [
						CommonsTableService
				]
		};
	}
}
