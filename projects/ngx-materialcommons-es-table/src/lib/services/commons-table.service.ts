import { Injectable } from '@angular/core';

import { Observable, ReplaySubject } from 'rxjs';

import { CommonsTableComponent } from '../components/commons-table/commons-table.component';

import { TCommonsTableColumn } from '../types/tcommons-table-column';

import { ECommonsTableSelectable } from '../enums/ecommons-table-selectable';

@Injectable()
export class CommonsTableService {
	private onColumnsChanged: Map<
			CommonsTableComponent,
			ReplaySubject<TCommonsTableColumn[]>
	> = new Map<
			CommonsTableComponent,
			ReplaySubject<TCommonsTableColumn[]>
	>();

	private onSelectable: Map<
			CommonsTableComponent,
			ReplaySubject<ECommonsTableSelectable|undefined>
	> = new Map<
			CommonsTableComponent,
			ReplaySubject<ECommonsTableSelectable|undefined>
	>();
	private selectable: Map<
			CommonsTableComponent,
			ECommonsTableSelectable|undefined
	> = new Map<
			CommonsTableComponent,
			ECommonsTableSelectable
	>();

	private onDisabled: Map<
			CommonsTableComponent,
			ReplaySubject<boolean>
	> = new Map<
			CommonsTableComponent,
			ReplaySubject<boolean>
	>();
	private disabled: Map<
			CommonsTableComponent,
			boolean
	> = new Map<
			CommonsTableComponent,
			boolean
	>();
	
	public registerTable(component: CommonsTableComponent): void {
		this.onColumnsChanged.set(component, new ReplaySubject<TCommonsTableColumn[]>(1));

		this.onSelectable.set(component, new ReplaySubject<ECommonsTableSelectable|undefined>(1));
		this.selectable.set(component, undefined);

		this.onDisabled.set(component, new ReplaySubject<boolean>(1));
		this.disabled.set(component, false);
	}
	
	public columnsChangedObservable(component: CommonsTableComponent): Observable<TCommonsTableColumn[]> {
		if (!this.onColumnsChanged.has(component)) throw new Error('Unregistered table');
		
		return this.onColumnsChanged.get(component)!;
	}
	
	public cascadeColumnsChanged(
			component: CommonsTableComponent,
			columns: TCommonsTableColumn[]
	): void {
		if (!this.onColumnsChanged.has(component)) throw new Error('Unregistered table');
		
		this.onColumnsChanged.get(component)!.next(columns);
	}
	
	public selectableObservable(component: CommonsTableComponent): Observable<ECommonsTableSelectable|undefined> {
		if (!this.onSelectable.has(component)) throw new Error('Unregistered table');
		
		return this.onSelectable.get(component)!;
	}
	
	public cascadeSelectable(
			component: CommonsTableComponent,
			selectable: ECommonsTableSelectable|undefined
	): void {
		if (!this.onSelectable.has(component)) throw new Error('Unregistered table');
		
		this.selectable.set(component, selectable);
		this.onSelectable.get(component)!.next(selectable);
	}
	
	public getSelectable(
			component: CommonsTableComponent
	): ECommonsTableSelectable|undefined {
		if (!this.selectable.has(component)) throw new Error('Unregistered table');
		
		return this.selectable.get(component)!;
	}
	
	public disabledObservable(component: CommonsTableComponent): Observable<boolean> {
		if (!this.onSelectable.has(component)) throw new Error('Unregistered table');
		
		return this.onDisabled.get(component)!;
	}
	
	public cascadeDisabled(
			component: CommonsTableComponent,
			disabled: boolean
	): void {
		if (!this.onDisabled.has(component)) throw new Error('Unregistered table');
		
		this.disabled.set(component, disabled);
		this.onDisabled.get(component)!.next(disabled);
	}
	
	public getDisabled(
			component: CommonsTableComponent
	): boolean {
		if (!this.disabled.has(component)) throw new Error('Unregistered table');
		
		return this.disabled.get(component)!;
	}
}
