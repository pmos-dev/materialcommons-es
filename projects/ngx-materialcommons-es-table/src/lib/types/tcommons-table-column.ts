export type TCommonsTableColumn = {
		name: string;
		width?: string;
};
