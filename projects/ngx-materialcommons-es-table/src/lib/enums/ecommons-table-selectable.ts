import { commonsTypeIsString } from 'tscommons-es-core';

export enum ECommonsTableSelectable {
		SINGLE = 'single',
		MULTIPLE = 'multiple'
}

export function toECommonsTableSelectable(type: string): ECommonsTableSelectable|undefined {
	switch (type) {
	case ECommonsTableSelectable.SINGLE.toString():
		return ECommonsTableSelectable.SINGLE;
	case ECommonsTableSelectable.MULTIPLE.toString():
		return ECommonsTableSelectable.MULTIPLE;
	}
	return undefined;
}

export function fromECommonsTableSelectable(type: ECommonsTableSelectable): string {
	switch (type) {
	case ECommonsTableSelectable.SINGLE:
		return ECommonsTableSelectable.SINGLE.toString();
	case ECommonsTableSelectable.MULTIPLE:
		return ECommonsTableSelectable.MULTIPLE.toString();
	}
	
	throw new Error('Unknown ECommonsTableSelectable');
}

export function isECommonsTableSelectable(test: unknown): test is ECommonsTableSelectable {
	if (!commonsTypeIsString(test)) return false;
	
	return toECommonsTableSelectable(test) !== undefined;
}

export function keyToECommonsTableSelectable(key: string): ECommonsTableSelectable {
	switch (key) {
	case 'SINGLE':
		return ECommonsTableSelectable.SINGLE;
	case 'MULTIPLE':
		return ECommonsTableSelectable.MULTIPLE;
	}
	
	throw new Error(`Unable to obtain ECommonsTableSelectable for key: ${key}`);
}

export const ECOMMONS_TABLE_SELECTABLES: ECommonsTableSelectable[] = Object.keys(ECommonsTableSelectable)
	.map((e: string): ECommonsTableSelectable => keyToECommonsTableSelectable(e));
