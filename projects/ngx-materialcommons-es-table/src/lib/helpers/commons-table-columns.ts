import { QueryList } from '@angular/core';

import { CommonsTableHeaderColumnComponent } from '../components/commons-table-header-column/commons-table-header-column.component';

import { TCommonsTableColumn } from '../types/tcommons-table-column';

export function commonsTableColumnsListColumns(
		contentChildren?: QueryList<CommonsTableHeaderColumnComponent>
): TCommonsTableColumn[] {
	if (!contentChildren) return [];
	
	const columns: TCommonsTableColumn[] = contentChildren
		.map((child: CommonsTableHeaderColumnComponent): TCommonsTableColumn => ({
				name: child.name!,
				width: child.width
		}));
			
	return columns;
}

export function commonsTableColumnsGetGridTemplateColumns(
		columns: TCommonsTableColumn[]
): string {
	const widths: string[] = columns
		.map((column: TCommonsTableColumn): string => column.width || '1fr');
	
	return widths.join(' ');
}
