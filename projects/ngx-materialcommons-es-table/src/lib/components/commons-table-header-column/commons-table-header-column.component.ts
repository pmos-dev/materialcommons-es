import { Component, AfterContentInit, Input, HostBinding, ElementRef } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-header-column]',
		templateUrl: './commons-table-header-column.component.html',
		styleUrls: ['./commons-table-header-column.component.less']
})
export class CommonsTableHeaderColumnComponent extends CommonsComponent implements AfterContentInit {
	@HostBinding('title') @Input() public title: string|undefined;

	@Input() public name: string|undefined;
	@Input() public width?: string;
	
	constructor(
			private ref: ElementRef
	) {
		super();
	}
	
	ngAfterContentInit(): void {
		if (!this.title) this.title = this.ref.nativeElement.innerText;
	}
}
