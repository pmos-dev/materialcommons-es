import { Component, OnInit, AfterContentInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { ContentChildren, QueryList } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsTableService } from '../../services/commons-table.service';

import {
		commonsTableColumnsGetGridTemplateColumns,
		commonsTableColumnsListColumns
} from '../../helpers/commons-table-columns';

import { TCommonsTableColumn } from '../../types/tcommons-table-column';

import { ECommonsTableSelectable } from '../../enums/ecommons-table-selectable';

import { CommonsTableHeaderColumnComponent } from '../commons-table-header-column/commons-table-header-column.component';
import { CommonsTableComponent } from '../commons-table/commons-table.component';

@Component({
		selector: '[commons-table-header-columns]',
		templateUrl: './commons-table-header-columns.component.html',
		styleUrls: ['./commons-table-header-columns.component.less']
})
export class CommonsTableHeaderColumnsComponent extends CommonsComponent implements OnInit, AfterContentInit {
	ECommonsTableSelectable = ECommonsTableSelectable;
	
	@HostBinding('style.gridTemplateColumns') hostGridTemplateColumns: string|undefined;
	@ContentChildren(CommonsTableHeaderColumnComponent) contentChildren: QueryList<CommonsTableHeaderColumnComponent>|undefined;
	
	@Input() selectAll: boolean = false;
	@Output() selectAllChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
	
	selectable: ECommonsTableSelectable|undefined;
	disabled: boolean = false;
	
	constructor(
			private root: CommonsTableComponent,
			private service: CommonsTableService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.service.selectableObservable(this.root),
				(selectable: ECommonsTableSelectable|undefined): void => {
					setTimeout(
							(): void => {
								this.selectable = selectable;
							},
							0
					);
				}
		);
		this.selectable = this.service.getSelectable(this.root);
		
		this.subscribe(
				this.service.disabledObservable(this.root),
				(disabled: boolean): void => {
					setTimeout(
							(): void => {
								this.disabled = disabled;
							},
							0
					);
				}
		);
		this.disabled = this.service.getDisabled(this.root);
	}
	
	ngAfterContentInit(): void {
		if (this.contentChildren) {
			this.subscribe(
					this.contentChildren.changes,
					(): void => {
						this.refreshColumns();
					}
			);
		}
		this.refreshColumns();
	}
	
	private refreshColumns(): void {
		const columns: TCommonsTableColumn[] = commonsTableColumnsListColumns(this.contentChildren);
		
		let widths: string = commonsTableColumnsGetGridTemplateColumns(columns);
		if (this.selectable) widths = `52px ${widths}`;
		
		setTimeout(
				(): void => {
					this.hostGridTemplateColumns = widths;
				},
				0
		);
	}
	
	doSelectAll(): void {
		this.selectAllChange.emit(this.selectAll);
	}
}
