import { Component, OnInit, AfterContentInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ContentChildren, QueryList } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsTableService } from '../../services/commons-table.service';

import { commonsTableColumnsListColumns } from '../../helpers/commons-table-columns';

import { TCommonsTableColumn } from '../../types/tcommons-table-column';

import { ECommonsTableSelectable } from '../../enums/ecommons-table-selectable';

import { CommonsTableHeaderColumnComponent } from '../commons-table-header-column/commons-table-header-column.component';
import { CommonsTableBodyComponent } from '../commons-table-body/commons-table-body.component';

@Component({
		selector: '[commons-table]',
		templateUrl: './commons-table.component.html',
		styleUrls: ['./commons-table.component.less']
})
export class CommonsTableComponent extends CommonsComponent implements OnInit, AfterContentInit, OnChanges {
	@ContentChildren(CommonsTableHeaderColumnComponent, { descendants: true }) columnsContentChildren: QueryList<CommonsTableHeaderColumnComponent>|undefined;
	@ContentChildren(CommonsTableBodyComponent) bodyContentChildren: QueryList<CommonsTableBodyComponent>|undefined;

	@Input() selectable: ECommonsTableSelectable|undefined;
	@Input() disabled: boolean = false;

	constructor(
			private service: CommonsTableService
	) {
		super();
		
		this.service.registerTable(this);
	}

	// these have to be public so that the other component classes can access them
	public columns: TCommonsTableColumn[] = [];
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.service.cascadeSelectable(this, this.selectable);
		this.service.cascadeDisabled(this, this.disabled);
	}
	
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.selectable && changes.selectable.currentValue !== changes.selectable.previousValue) {
			this.service.cascadeSelectable(this, this.selectable);
		}
		if (changes.disabled && changes.disabled.currentValue !== changes.disabled.previousValue) {
			this.service.cascadeDisabled(this, this.disabled);
		}
	}
	
	ngAfterContentInit(): void {
		if (this.columnsContentChildren) {
			this.subscribe(
					this.columnsContentChildren.changes,
					(): void => {
						this.refreshColumns();
					}
			);
		}
		this.refreshColumns();
	}
	
	private refreshColumns(): void {
		this.columns = commonsTableColumnsListColumns(this.columnsContentChildren);
		
		this.service.cascadeColumnsChanged(this, this.columns);
	}
}
