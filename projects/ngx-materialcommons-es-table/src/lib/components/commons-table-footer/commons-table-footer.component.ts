import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-footer]',
		templateUrl: './commons-table-footer.component.html',
		styleUrls: ['./commons-table-footer.component.less']
})
export class CommonsTableFooterComponent extends CommonsComponent {}
