import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-header]',
		templateUrl: './commons-table-header.component.html',
		styleUrls: ['./commons-table-header.component.less']
})
export class CommonsTableHeaderComponent extends CommonsComponent {}
