import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-footer-pagination]',
		templateUrl: './commons-table-footer-pagination.component.html',
		styleUrls: ['./commons-table-footer-pagination.component.less']
})
export class CommonsTableFooterPaginationComponent extends CommonsComponent implements OnChanges {
	@Input() from: number|undefined;
	@Input() total: number|undefined;
	
	@Input() rows: number|undefined;
	@Output() private rowsChange: EventEmitter<number> = new EventEmitter<number>(true);

	@Output() firstPage: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() prevPage: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() nextPage: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() lastPage: EventEmitter<void> = new EventEmitter<void>(true);
	
	internalRows: string|undefined;
	canBack: boolean|undefined;
	canForward: boolean|undefined;
	
	doChangeRows(): void {
		if (this.internalRows === undefined) return;
		
		this.rowsChange.emit(parseInt(this.internalRows, 10));
	}
	
	ngOnChanges(_changes: SimpleChanges): void {
		this.internalRows = this.rows === undefined ? undefined : this.rows.toString();
		
		const thisPageEnd: number = (this.from || 0) + (this.rows || 0);

		this.canBack = this.rows !== undefined
				&& this.from !== undefined
				&& this.total !== undefined
				&& this.total > 0
				&& this.from > 0;
		this.canForward = this.rows !== undefined
				&& this.total !== undefined
				&& this.total > 0
				&& thisPageEnd < this.total;
	}
}
