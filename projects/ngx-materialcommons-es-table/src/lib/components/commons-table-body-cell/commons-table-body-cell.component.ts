import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-body-cell]',
		templateUrl: './commons-table-body-cell.component.html',
		styleUrls: ['./commons-table-body-cell.component.less']
})
export class CommonsTableBodyCellComponent extends CommonsComponent {}
