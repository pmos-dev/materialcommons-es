import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-header-filter]',
		templateUrl: './commons-table-header-filter.component.html',
		styleUrls: ['./commons-table-header-filter.component.less']
})
export class CommonsTableHeaderFilterComponent extends CommonsComponent {}
