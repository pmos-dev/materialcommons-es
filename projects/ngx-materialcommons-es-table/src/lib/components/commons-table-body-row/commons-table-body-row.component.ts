import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsTableService } from '../../services/commons-table.service';

import { commonsTableColumnsGetGridTemplateColumns } from '../../helpers/commons-table-columns';

import { TCommonsTableColumn } from '../../types/tcommons-table-column';

import { ECommonsTableSelectable } from '../../enums/ecommons-table-selectable';

import { CommonsTableComponent } from '../commons-table/commons-table.component';

@Component({
		selector: '[commons-table-body-row]',
		templateUrl: './commons-table-body-row.component.html',
		styleUrls: ['./commons-table-body-row.component.less']
})
export class CommonsTableBodyRowComponent extends CommonsComponent implements OnInit {
	@HostBinding('style.gridTemplateColumns') hostGridTemplateColumns: string|undefined;
	@HostBinding('class.selected') @Input() selected: boolean = false;
	@Output() selectedChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

	selectable: ECommonsTableSelectable|undefined;
	disabled: boolean = false;

	constructor(
			private root: CommonsTableComponent,
			private service: CommonsTableService
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.service.selectableObservable(this.root),
				(selectable: ECommonsTableSelectable|undefined): void => {
					setTimeout(
							(): void => {
								this.selectable = selectable;
							},
							0
					);
				}
		);
		this.selectable = this.service.getSelectable(this.root);
		
		this.subscribe(
				this.service.disabledObservable(this.root),
				(disabled: boolean): void => {
					setTimeout(
							(): void => {
								this.disabled = disabled;
							},
							0
					);
				}
		);
		this.disabled = this.service.getDisabled(this.root);

		this.subscribe(
				this.service.columnsChangedObservable(this.root),
				(columns: TCommonsTableColumn[]): void => {
					let widths: string = commonsTableColumnsGetGridTemplateColumns(columns);
					if (this.selectable !== undefined) widths = `52px ${widths}`;

					setTimeout(
							(): void => {
								this.hostGridTemplateColumns = widths;
							},
							0
					);
				}
		);
	}
	
	doSelected(): void {
		this.selectedChange.emit(this.selected);
	}
}
