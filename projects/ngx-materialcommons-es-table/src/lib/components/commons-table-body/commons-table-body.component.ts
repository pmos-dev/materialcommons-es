import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

@Component({
		selector: '[commons-table-body]',
		templateUrl: './commons-table-body.component.html',
		styleUrls: ['./commons-table-body.component.less']
})
export class CommonsTableBodyComponent extends CommonsComponent {}
