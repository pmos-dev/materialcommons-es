import { Component, OnInit, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { CommonsSnackService } from 'ngx-materialcommons-es-mobile';

import { CommonsLockService, TShowLock } from '../../services/commons-lock.service';

@Component({
		selector: 'commons-pin-locker',
		templateUrl: './commons-pin-locker.component.html',
		styleUrls: ['./commons-pin-locker.component.less']
})
export class CommonsPinLockerComponent extends CommonsComponent implements OnInit {
	@Input() minLength: number = 1;
	@Input() maxLength: number = 4;

	title?: string;
	pin?: string;

	constructor(
			private snackService: CommonsSnackService,
			private lockService: CommonsLockService
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.lockService.observable(),
				(lock: TShowLock|undefined): void => {
					if (lock === undefined) {
						this.title = undefined;
						this.pin = undefined;
					} else {
						this.title = lock.title;
						this.pin = lock.pin;
					}
				}
		);
	}

	doCheckPin(entered: string): void {
		if (entered !== this.pin) {
			this.snackService.error('Invalid pin');
			return;
		}
		
		this.lockService.unlock();
	}
}
