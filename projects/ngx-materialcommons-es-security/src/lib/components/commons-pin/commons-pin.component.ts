import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-es-core';

import { ICommonsKeyboard } from 'ngx-materialcommons-es-keyboard';
import { LOGIN_PIN_KEYBOARD } from 'ngx-materialcommons-es-keyboard';

@Component({
		selector: 'commons-pin',
		templateUrl: './commons-pin.component.html',
		styleUrls: ['./commons-pin.component.less']
})
export class CommonsPinComponent extends CommonsComponent implements OnInit {
	pinKeyboard: ICommonsKeyboard = LOGIN_PIN_KEYBOARD;
	
	@Input() title?: string;
	@Input() hideDigits: boolean = true;
	@Input() minLength: number = 1;
	@Input() maxLength: number = 4;

	@Output() onSubmit: EventEmitter<string> = new EventEmitter<string>(true);
	
	pin: string = '';
	showDigit: boolean = false;
	timeout: any;
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.pin = '';
		this.showDigit = !this.hideDigits;
	}

	doCharacter(digit: string): void {
		this.pin = this.pin + digit;
		
		if (this.hideDigits) {
			if (this.timeout) clearTimeout(this.timeout);
	
			this.showDigit = true;
			this.timeout = this.setTimeout(
					1000,
					async (): Promise<void> => {
						this.showDigit = false;
					}
			);
		}
	}
}
