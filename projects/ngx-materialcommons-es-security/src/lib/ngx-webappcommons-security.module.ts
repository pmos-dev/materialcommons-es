import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMaterialCommonsEsAppModule } from 'ngx-materialcommons-es-app';
import { NgxMaterialCommonsEsMobileModule } from 'ngx-materialcommons-es-mobile';
import { NgxMaterialCommonsEsKeyboardModule } from 'ngx-materialcommons-es-keyboard';

import { CommonsPinLockerComponent } from './components/commons-pin-locker/commons-pin-locker.component';
import { CommonsPinComponent } from './components/commons-pin/commons-pin.component';
import { CommonsPinLoginComponent } from './components/commons-pin-login/commons-pin-login.component';

import { CommonsLockService } from './services/commons-lock.service';

@NgModule({
		imports: [
				CommonModule,
				NgxMaterialCommonsEsAppModule,
				NgxMaterialCommonsEsMobileModule,
				NgxMaterialCommonsEsKeyboardModule
		],
		declarations: [
				CommonsPinComponent,
				CommonsPinLockerComponent,
				CommonsPinLoginComponent
		],
		exports: [
				CommonsPinComponent,
				CommonsPinLockerComponent,
				CommonsPinLoginComponent
		]
})
export class NgxMaterialCommonsEsSecurityModule {
	static forRoot(): ModuleWithProviders<NgxMaterialCommonsEsSecurityModule> {
		return {
				ngModule: NgxMaterialCommonsEsSecurityModule,
				providers: [
						CommonsLockService
				]
		};
	}
}
