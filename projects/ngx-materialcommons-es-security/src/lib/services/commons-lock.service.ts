import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

export type TShowLock = {
		title: string;
		pin: string;
};

@Injectable()
export class CommonsLockService {
	private emitter: EventEmitter<TShowLock|undefined> = new EventEmitter<TShowLock|undefined>(true);

	public observable(): Observable<TShowLock|undefined> {
		return this.emitter.asObservable();
	}

	public lock(title: string, pin: string): void {
		this.emitter.emit({
				title: title,
				pin: pin
		});
	}

	public unlock(): void {
		this.emitter.emit(undefined);
	}
}
