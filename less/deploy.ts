import { CommonsFile } from 'nodecommons-file';

const themes: string[] = CommonsFile.listFilesWithExtensions('./themes', 'less')
		.map((file: string): string => file.replace(/\.less$/, ''));

const code: string[] = [ `@charset 'utf-8';\n\n`, `@md_themes: ${themes.join(', ')};\n` ];
for (const theme of themes) {
	code.push(`
/* ${theme} theme */
@import (less) './themes/${theme}.less';

@md_theme-${theme}_body--background--greyscale: greyscale(@theme-${theme}_body--background);
@md_theme-${theme}_body--background--greyscale--invert: difference(@md_theme-${theme}_body--background--greyscale, #ffffff);
@md_theme-${theme}_body--text: mix(@md_theme-${theme}_body--background--greyscale--invert, @md_theme-${theme}_body--background--greyscale, 80%);
@md_theme-${theme}_body--text--invert: difference(@md_theme-${theme}_body--text, #ffffff);

@md_theme-${theme}_modal--background: fade(@md_theme-${theme}_body--background--greyscale--invert, 50%);
@md_theme-${theme}_modal--background--dark: fade(@md_theme-${theme}_body--background--greyscale, 50%);

@md_theme-${theme}_disabled: mix(@md_theme-${theme}_body--text, @md_theme-${theme}_body--background--greyscale, 30%);
@md_theme-${theme}_subtle: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert, 60%);
@md_theme-${theme}_subtler: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert, 75%);
@md_theme-${theme}_subtlerer: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert,88%);
@md_theme-${theme}_subtlest: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert, 95%);

@md_theme-${theme}_subtle--dark: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert, 40%);
@md_theme-${theme}_subtler--dark: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert, 25%);
@md_theme-${theme}_subtlest--dark: mix(@md_theme-${theme}_body--background--greyscale, @md_theme-${theme}_body--background--greyscale--invert, 5%);

@md_theme-${theme}_shadow: fade(@md_theme-${theme}_body--background--greyscale--invert, 50%);
@md_theme-${theme}_shadow-subtle: fade(@md_theme-${theme}_body--background--greyscale--invert, 30%);

@md_theme-${theme}_shadow--dark: fade(@md_theme-${theme}_body--background--greyscale, 50%);
@md_theme-${theme}_shadow-subtle--dark: fade(@md_theme-${theme}_body--background--greyscale, 30%);
`);
}

console.log(code.join('\n'));
